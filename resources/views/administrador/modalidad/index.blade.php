@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Modalidades</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            @can('modalidad.create')
                                <button type="button" class="btn btn-success" onclick="$('#RegistrarModalidad').modal('show');" >Agregar modalidad</button>
                            @endcan
                                <table class="table table-striped" id="myTable" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre de la modalidad</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($modalidades as $modalidad)
                                    <tr>
                                        <td>{{ $modalidad->id }}</td>
                                        <td>{{ $modalidad->modalidad }}</td>
                                        <td>
                                            @can('modalidad.edit')
                                                <button type="button" class="btn btn-warning" onclick="$('#{{ $modalidad->id }}').modal('show');">Editar</button>
                                                <div id="{{ $modalidad->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Editar modalidad</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="POST" action="{{ route('modalidad.update',$modalidad->id) }}">
                                                                    @csrf
                                                                    @method('PUT') 
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Nuevo nombre del modalidad</label>
                                                                                    <input type="text" class="form-control" id="modalidad" name="modalidad" value="{{ $modalidad->modalidad }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row justify-content-end">
                                                                            <button type="submit" class="btn btn-warning">
                                                                                Editar
                                                                            </button>
                                                                            <p></p>
                                                                            <button class="btn btn-danger" data-bs-dismiss="modal" >
                                                                                Cancelar
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@can('modalidad.create')
<div id="RegistrarModalidad" class="modal fade" tabindex="-1"> 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrar modalidad</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
        <form id="form-create" action="{{ url('modalidad') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Nombre del modalidad</label>
                        <input type="text" class="form-control" id="modalidad" name="modalidad" required>
                    </div>
                </div>
            </div>
            <div class="row justify-content-end">
                <button type="submit" class="btn btn-success">
                    Registrar
                </button>
                <p></p>
                <button class="btn btn-danger" data-bs-dismiss="modal" >
                    Cancelar
                </button>
            </div>
        </form>
    </div>
</div>
@endcan
@endsection
