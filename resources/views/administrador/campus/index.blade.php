@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Campus</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                        @can('campus.create')
                            <button type="button" class="btn btn-success" onclick="$('#RegistrarCampus').modal('show');" >Agregar campus</button>
                        @endcan
                            <table class="table table-striped" id="myTable" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre del campus</th>
                                        <th>Editar</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($campus as $c)
                                    <tr>
                                        <td>{{ $c->id }}</td>
                                        <td>{{ $c->campus }}</td>
                                        <td>
                                            @can('campus.edit')
                                                <button type="button" class="btn btn-warning" onclick="$('#{{ $c->id }}').modal('show');">Editar</button>
                                                <div id="{{ $c->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Editar campus</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="POST" action="{{ route('campus.update',$c->id) }}">
                                                                    @csrf
                                                                    @method('PUT') 
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Nuevo nombre del campus</label>
                                                                                    <input type="text" class="form-control" id="campus" name="campus" value="{{ $c->campus }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row justify-content-end">
                                                                            <button type="submit" class="btn btn-warning">
                                                                                Editar
                                                                            </button>
                                                                        </div>
                                                                    
                                                                </form>
                                                                <p></p>
                                                                <div class="row justify-content-end">
                                                                    <button class="btn btn-danger" data-bs-dismiss="modal" >
                                                                        Cancelar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                           
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@can('campus.create')
<div id="RegistrarCampus" class="modal fade" tabindex="-1"> 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrar Campus</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
        <form id="form-create" action="{{ url('campus') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Nombre del campus</label>
                        <input type="text" class="form-control" id="campus" name="campus" required>
                    </div>
                </div>
            </div>
            <div class="row justify-content-end">
                <button type="submit" class="btn btn-success">
                    Registrar
                </button>
                <p></p>
                <button class="btn btn-danger" data-bs-dismiss="modal" >
                    Cancelar
                </button>
            </div>
        </form>
    </div>
</div>
@endcan
@endsection
