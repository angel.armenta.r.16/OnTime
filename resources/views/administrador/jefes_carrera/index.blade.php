@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Jefes de carrera</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card" >
                        <div class="card-body">
                            @can('jefecarrera.create')
                                <button type="button" class="btn btn-success" onclick="$('#AsignarJefeCarrera').modal('show');" >Asignar jefe de carrera</button>
                            @endcan
                            <table class="table table-striped" id="myTable" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Número de personal</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Carrera</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jefes_carrera as $jefe)
                                    <tr>
                                        <td>{{ $jefe->num_personal }}</td>
                                        <td>{{ $jefe->nombre }}</td>
                                        <td>{{ $jefe->apellido_p }}</td>
                                        <td>{{ $jefe->apellido_m }}</td>
                                        <td>{{ $jefe->carrera }}</td>
                                        <td>
                                            @can('jefecarrera.destroy')
                                            <button type="button" class="btn btn-danger" onclick="$('#eliminar_{{ $jefe->id }}').modal('show');">Eliminar</button>
                                                <div id="eliminar_{{ $jefe->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Eliminar maestro</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h5>¿Desea eliminar destituir al maestro {{ $jefe->num_personal }} - {{ $jefe->nombre }} {{ $jefe->apellido_p }} {{ $jefe->apellido_m }} de la carrera {{ $jefe->carrera }}?</h5>
                                                                <form action="{{ route('jefescarrera.destroy' , $jefe->id ) }}" method="post" class="d-inline">
                                                                    @csrf
                                                                    @method("DELETE")
                                                                    <div class="row justify-content-end">
                                                                        <button type="submit" class="btn btn-danger">
                                                                            Eliminar
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                                <p></p>
                                                                <div class="row justify-content-end">
                                                                    <button class="btn btn-dark" data-bs-dismiss="modal" >
                                                                        Cancelar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $jefes_carrera->links() }}
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@can('jefecarrera.create')
    <div id="AsignarJefeCarrera" class="modal fade" tabindex="-1"> 
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Asignar jefe de carrera</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="form-create" action="{{ url('jefescarrera') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Maestro</label>
                            <select class="form-control" id="usuario_id" name="usuario_id">
                                <option selected>Elige un maestro...</option>
                                @foreach($maestros as $maestro)    
                                    <option value="{{ $maestro->usuario_id }}">{{ $maestro->num_personal }} - {{ $maestro->nombre }} {{ $maestro->apellido_p }} {{ $maestro->apellido_m }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Carrera</label>
                            <select class="form-control" id="carrera_id" name="carrera_id">
                                <option selected>Elige una carrera...</option>
                                @foreach($carreras as $c)    
                                    <option value="{{ $c->id }}">{{ $c->carrera }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <button type="submit" class="btn btn-success">
                        Asignar
                    </button>
                    <p></p>
                    <button class="btn btn-danger" data-bs-dismiss="modal">
                        Cancelar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endcan
@endsection
