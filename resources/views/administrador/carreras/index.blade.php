@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Carrera</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            @can('carrera.create')
                                <button type="button" class="btn btn-success" onclick="$('#RegistrarCarrera').modal('show');" >Agregar carrera</button>
                            @endcan
                            <table class="table table-striped" id="myTable" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre de la carrera</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($carreras as $carrera)
                                    <tr>
                                        <td>{{ $carrera->id }}</td>
                                        <td>{{ $carrera->carrera }}</td>
                                        <td>
                                            @can('carrera.edit')
                                                <button type="button" class="btn btn-warning" onclick="$('#{{ $carrera->id }}').modal('show');">Editar</button>
                                                <div id="{{ $carrera->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Editar carrera</h5>
                                                                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="POST" action="{{ route('carrera.update',$carrera->id) }}">
                                                                    @csrf
                                                                    @method('PUT') 
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Nuevo nombre de la carrera</label>
                                                                                    <input type="text" class="form-control" id="carrera" name="carrera" value="{{ $carrera->carrera }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row justify-content-end">
                                                                            <button type="submit" class="btn btn-danger" data-dismiss="modal" >
                                                                                Cancelar
                                                                            </button>
                                                                            <button type="submit" class="btn btn-success">
                                                                                Editar
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            No hay registros
                                            </td> 
                                            <td></td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="RegistrarCarrera" class="modal fade" tabindex="-1"> 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrar Campus</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
        <form id="form-create" action="{{ url('carrera') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Nombre de la carrera</label>
                        <input type="text" class="form-control" id="carrera" name="carrera" required>
                    </div>
                </div>
            </div>
            <div class="row justify-content-end">
                <button type="submit" class="btn btn-danger" data-dismiss="modal" >
                    Cancelar
                </button>

                <p></p>
                <button type="submit" class="btn btn-success">
                    Registrar
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
