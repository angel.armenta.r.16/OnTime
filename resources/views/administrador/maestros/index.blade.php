@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Maestros</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                   
                    <div class="card">
                        <div class="card-body">
                            @can('maestro.create')
                                <button type="button" class="btn btn-success" onclick="$('#RegistrarMaestro').modal('show');" >Registrar maestro</button>
                            @endcan
                            <table class="table table-striped" id="myTable" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Número de personal</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($maestros as $maestro)
                                    <tr>
                                        <td>{{ $maestro->numero_personal }}</td>
                                        <td>{{ $maestro->nombre }}</td>
                                        <td>{{ $maestro->apellido_p }}</td>
                                        <td>{{ $maestro->apellido_m }}</td>
                                        <td>
                                            @can('maestro.edit')
                                                <button type="button" class="btn btn-warning" onclick="$('#{{ $maestro->id }}').modal('show');">Editar</button>
                                                <div id="{{ $maestro->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Editar maestro</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="POST" action="{{ route('maestro.update',$maestro->id) }}">
                                                                    @csrf
                                                                    @method('PUT') 
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Nuevo número de personal</label>
                                                                                    <input type="number" class="form-control" id="numero_personal" name="numero_personal" value="{{ $maestro->numero_personal }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Nombre del maestro</label>
                                                                                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $maestro->nombre }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Apellido paterno del maestro</label>
                                                                                    <input type="text" class="form-control" id="apellido_p" name="apellido_p" value="{{ $maestro->apellido_p }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Apellido materno del maestro</label>
                                                                                    <input type="text" class="form-control" id="apellido_m" name="apellido_m" value="{{ $maestro->apellido_m }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Correo electrónico institucional</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <input type="text" class="form-control" id="email" name="email" value="{{ $maestro->id }}" required>
                                                                                        <span class="input-group-text" id="basic-addon2">@utgz.edu.mx</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row justify-content-end">
                                                                            <button type="submit" class="btn btn-success">
                                                                                Editar
                                                                            </button>
                                                                        </div>
                                                                </form>
                                                                <p></p>
                                                                <div class="row justify-content-end">
                                                                    <button class="btn btn-danger" data-bs-dismiss="modal" >
                                                                        Cancelar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                                        <td>
                                            @can('maestro.destroy')
                                            <button type="button" class="btn btn-danger" onclick="$('#eliminar_{{ $maestro->id }}').modal('show');">Eliminar</button>
                                                <div id="eliminar_{{ $maestro->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Eliminar maestro</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h5>¿Desea eliminar al maestro {{ $maestro->numero_personal }} - {{ $maestro->nombre }} {{ $maestro->apellido_p }} {{ $maestro->apellido_m }}?</h5>
                                                                <form action="{{ route('maestro.destroy' , $maestro->id ) }}" method="post" class="d-inline">
                                                                    @csrf
                                                                    @method("DELETE")
                                                                    <div class="row justify-content-end">
                                                                        <button type="submit" class="btn btn-danger">
                                                                            Eliminar
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                                <p></p>
                                                                <div class="row justify-content-end">
                                                                    <button class="btn btn-dark" data-bs-dismiss="modal" >
                                                                        Cancelar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $maestros->links() }}
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@can('maestro.create')
    <div id="RegistrarMaestro" class="modal fade" tabindex="-1"> 
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Registrar maestro</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="form-create" action="{{ url('maestro') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Número de personal</label>
                            <input type="number" class="form-control" id="numero_personal" name="numero_personal" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Nombre del maestro</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Apellido paterno del maestro</label>
                            <input type="text" class="form-control" id="apellido_p" name="apellido_p" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Apellido materno del maestro</label>
                            <input type="text" class="form-control" id="apellido_m" name="apellido_m" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Correo electrónico institucional</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="email" name="email" required>
                                <span class="input-group-text" id="basic-addon2">@utgz.edu.mx</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <button type="submit" class="btn btn-success">
                        Registrar
                    </button>
                    <p></p>
                    <button class="btn btn-danger" data-bs-dismiss="modal">
                        Cancelar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endcan
@endsection
