@extends('adminlte::page')

@section('content')
<div class="container text-center">
	<div class="row justify-content-center">
		<div class="card shadow  bg-body rounded">
			<div class="card-header" align="center" style="background-color:#E74C3C;"><font color="white"  size=3>{{ $maestro->numero_personal }}</font></div>
				<div class="card-body">
					<div class="row">
						<div class="col-md">
							<div class="input-group mb-2">
  								<span class="input-group-text col-3" id="basic-addon1">Nombre</span>
								<label class=" form-control border mr-auto" align="left">{{ $maestro->nombre }}</label>
							</div>
                        </div>
                        <div class="col-md">
                            <div class="input-group mb-2">
  								<span class="input-group-text col-3" id="basic-addon1">Apellido paterno</span>
								<label class=" form-control border mr-auto" align="left">{{ $maestro->apellido_p }}</label>
							</div>
                        </div>
                        <div class="col-md">
                            <div class="input-group mb-2">
  								<span class="input-group-text col-3" id="basic-addon1">Apellido materno</span>
								<label class=" form-control border mr-auto" align="left">{{ $maestro->apellido_m }}</label>
							</div>
						</div>
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="5"><font size=3> <i class="fa fa-building"></i> Grupos de  {{ $maestro->nombre }}</font></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($grupos_asignados as $grupo)
                                            <tr>
                                                <td> {{ $grupo-> grupo }} - {{ $grupo-> cuatrimestre }} {{ $grupo-> año }}</td>
                                        
                                            </tr>
                                            @empty
                                            <tr>
                                                <td>
                                                No tiene grupos
                                            </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if(Session::get('mensaje'))
                            <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                {{ Session::get('mensaje') }}
                            </div>
                        @endif
                    </div>
					<br>
					<div class="row" align="center">
						<div class="col">
                            <br>
                            <a class="btn btn-info" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Regresar</a>
						</div>
					</div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection