@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Sistema</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            @can('sistema.create')
                                <button type="button" class="btn btn-success" onclick="$('#RegistrarSistema').modal('show');" >Agregar sistema</button>
                            @endcan
                            <table class="table table-striped" id="myTable" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre del sistema</th>
                                        <th>Editar</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sistemas as $sistema)
                                    <tr>
                                        <td>{{ $sistema->id }}</td>
                                        <td>{{ $sistema->sistema }}</td>
                                        <td>
                                            @can('sistema.edit')
                                                <button type="button" class="btn btn-warning" onclick="$('#{{ $sistema->id }}').modal('show');">Editar</button>
                                                <div id="{{ $sistema->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Editar sistema</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="POST" action="{{ route('sistema.update',$sistema->id) }}">
                                                                    @csrf
                                                                    @method('PUT') 
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Nuevo nombre del sistema</label>
                                                                                    <input type="text" class="form-control" id="sistema" name="sistema" value="{{ $sistema->sistema }}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row justify-content-end">
                                                                            <button type="submit" class="btn btn-warning">
                                                                                Editar
                                                                            </button>
                                                                            <p></p>
                                                                            <button type="submit" class="btn btn-danger" data-bs-dismiss="modal" >
                                                                                Cancelar
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan
                                        </td>
                                    </tr>
                                   @endforeach
                            
                                </tbody>
                            </table>
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@can('sistema.create')
    <div id="RegistrarSistema" class="modal fade" tabindex="-1"> 
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Registrar sistema</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="form-create" action="{{ url('sistema') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="name">Nombre del sistema</label>
                            <input type="text" class="form-control" id="sistema" name="sistema" required>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <button type="submit" class="btn btn-success">
                        Registrar
                    </button>
                    <p></p>
                    <button class="btn btn-danger" data-bs-dismiss="modal" >
                        Cancelar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endcan
@endsection
