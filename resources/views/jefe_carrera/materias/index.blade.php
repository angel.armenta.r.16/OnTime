@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Materias de la carrera {{ $carrera }}</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-success" onclick="$('#RegistrarMateria').modal('show');" >Registrar materia</button>
                            <table class="table table-striped" style="width:100%;border:1px;" id="myTable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre de la materia</th>
                                        <th>Cuatrimestre</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($materias as $materia)
                                    <tr>
                                        <td>{{ $materia->materia_id }}</td>
                                        <td>{{ $materia->materia }}</td>
                                        <td>{{ $materia->cuatrimestre }}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning" onclick="$('#{{ $materia->id }}').modal('show');">Editar</button>

                                            <div id="{{ $materia->id }}" class="modal fade" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Editar materia</h5>
                                                            <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form method="POST" action="{{ route('materia.update',$materia->materia_id) }}">
                                                                @csrf
                                                                @method('PUT') 
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Nuevo nombre de la materia</label>
                                                                                <input type="text" class="form-control" id="materia" name="materia" value="{{ $materia->materia }}" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Carrera a la que pertenece</label>
                                                                                <input type="text" class="form-control" id="carrera" name="carrera" value="{{ $carrera }}"required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Cuatrimestre al que pertenece</label>
                                                                                {{ $materia->cuatrimestre }}
                                                                                <select class="form-control" id="cuatrimestre" name="cuatrimestre">
                                                                                    <option value="{{ $materia->cuatrimestre }}" selected>{{ $materia->cuatrimestre }}</option>
                                                                                    <option value="1°">1°</option>
                                                                                    <option value="2°">2°</option>
                                                                                    <option value="3°">3°</option>
                                                                                    <option value="4°">4°</option>
                                                                                    <option value="5°">5°</option>
                                                                                    <option value="6°">6°</option>
                                                                                    <option value="7°">7°</option>
                                                                                    <option value="8°">8°</option>
                                                                                    <option value="9°">9°</option>
                                                                                    <option value="10°">10°</option>
                                                                                    <option value="11°">11°</option>
                                                                                    <option value="12°">12°</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row justify-content-end">
                                                                        <button class="btn btn-danger" data-bs-dismiss="modal" >
                                                                            Cancelar
                                                                        </button>
                                                                        <button type="submit" class="btn btn-success">
                                                                            Editar
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <form action="{{ route('materia.destroy' , $materia->id ) }}" method="post" class="d-inline">
                                                @csrf
                                                @method("DELETE")
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            No hay materias registradas
                                            </td> 
                                            <td></td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="RegistrarMateria" class="modal fade" tabindex="-1"> 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrar Materia</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
        <form id="form-create" action="{{ url('materia') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Nombre de la materia</label>
                        <input type="text" class="form-control" id="materia" name="materia" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Carrera a la que pertenece</label>
                        <input type="text" class="form-control" id="carrera" name="carrera" value="{{ $carrera }}"required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Cuatrimestre al que pertenece</label>
                        <select class="form-control" id="cuatrimestre" name="cuatrimestre">
                            <option selected>Elige el cuatrimestre</option>
                            <option value="1°">1°</option>
                            <option value="2°">2°</option>
                            <option value="3°">3°</option>
                            <option value="4°">4°</option>
                            <option value="5°">5°</option>
                            <option value="6°">6°</option>
                            <option value="7°">7°</option>
                            <option value="8°">8°</option>
                            <option value="9°">9°</option>
                            <option value="10°">10°</option>
                            <option value="11°">11°</option>
                            <option value="12°">12°</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row justify-content-end">
                <button class="btn btn-danger" data-bs-dismiss="modal">
                    Cancelar
                </button>

                <p></p>
                <button type="submit" class="btn btn-success">
                    Registrar
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
