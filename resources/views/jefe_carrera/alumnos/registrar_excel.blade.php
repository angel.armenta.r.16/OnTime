@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Registro de alumnos</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    @if (isset($errors) && $errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}
                                            @endforeach
                                        </div>
                                    @endif

                                    @if (session()->has('failures'))
                                        <table class="table table-danger">
                                            <tr>
                                                <th>Row</th>
                                                <th>Attribute</th>
                                                <th>Errors</th>
                                                <th>Value</th>
                                            </tr>

                                            @foreach (session()->get('failures') as $validation)
                                                <tr>
                                                    <td>{{ $validation->row() }}</td>
                                                    <td>{{ $validation->attribute() }}</td>
                                                    <td>
                                                        <ul>
                                                            @foreach ($validation->errors() as $e)
                                                                <li>{{ $e }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        {{ $validation->values()[$validation->attribute()] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    @endif
                                    <form action="/alumnos/import" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <input class="form-control" type="file" name="file" id="file" required>
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </form>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
