@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Registro de alumnos</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    @if(Session::get('mensaje'))
                                        <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                            {{ Session::get('mensaje') }}
                                        </div>
                                    @endif
                                    <form id="form-create" action="{{ url('alumno') }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Matrícula</label>
                                                    <input type="number" class="form-control" id="matricula" name="matricula" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Nombre</label>
                                                    <input type="text" class="form-control" id="nombre" name="nombre" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Apellido paterno</label>
                                                    <input type="text" class="form-control" id="apellido_p" name="apellido_p" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Apellido materno</label>
                                                    <input type="text" class="form-control" id="apellido_m" name="apellido_m" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Número de seguro social</label>
                                                    <input type="text" class="form-control" id="numero_ss" name="numero_ss" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Carrera</label>
                                                    <select class="form-control" id="carrera_id" name="carrera_id">
                                                        <option selected>Elige una carrera</option>
                                                        @foreach($carrera as $c)    
                                                            <option value="{{ $c->carrera }}">{{ $c->carrera }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <button type="submit" class="btn btn-success">
                                                Registrar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
