@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Alumnos</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <form action="/alumnos/credencial" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="name">Busca al alumno</label>
                                            <select class="form-control" id="select-alumnos" name="matricula">
                                                <option selected>Buscar...</option>
                                                @foreach($alumnos as $alumno)    
                                                    <option value="{{ $alumno->matricula }}">{{ $alumno->matricula }} - {{ $alumno->nombre }} {{ $alumno->apellido_p }} {{ $alumno->apellido_m }}</option>
                                                @endforeach
                                            </select>
                                            <button type="submit" class="btn btn-info">Mostrar</button>
                                        </div>
                                    </form>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
    $('#select-alumnos').select2();
    });
</script>
@endsection
