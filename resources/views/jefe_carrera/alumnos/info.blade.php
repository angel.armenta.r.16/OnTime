@extends('layouts.sidebar')
@section('content')
<div class="container">
    <h3>Alumno info</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group mb-2">
                                                <span class="input-group-text col-5" id="basic-addon1">Matricula</span>
                                                <label class=" form-control border mr-auto" align="left">{{ $alumno->matricula }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group mb-2">
                                                <span class="input-group-text col-5" id="basic-addon1">Nombre</span>
                                                <label class=" form-control border mr-auto" align="left">{{ $alumno->matricula }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group mb-2">
                                                <span class="input-group-text col-5" id="basic-addon1">Apellido Paterno</span>
                                                <label class=" form-control border mr-auto" align="left">{{ $alumno->apellido_p }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group mb-2">
                                                <span class="input-group-text col-5" id="basic-addon1">Apellido materno</span>
                                                <label class=" form-control border mr-auto" align="left">{{ $alumno->apellido_m }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group mb-2">
                                                <span class="input-group-text col-5" id="basic-addon1">Número de Seguro Social</span>
                                                <label class=" form-control border mr-auto" align="left">{{ $alumno->numero_ss }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group mb-2">
                                                <span class="input-group-text col-5" id="basic-addon1">Carrera</span>
                                                <label class=" form-control border mr-auto" align="left">{{ $alumno->carrera }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <button type="button" class="btn btn-warning" onclick="$('#editar').modal('show');">Editar</button>
                                    </div>
                                    <div class="row">

                                    </div>
                                </div>    
                            </div>            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="editar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar grupo</h5>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('alumno.update',$alumno->matricula) }}">
                    @csrf
                    @method('PUT') 
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="name">Matrícula</label>
                                <input type="number" class="form-control" id="matricula" name="matricula" value="{{ $alumno->matricula }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $alumno->nombre }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="name">Apellido paterno</label>
                                <input type="text" class="form-control" id="apellido_p" name="apellido_p" value="{{ $alumno->apellido_p }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="name">Apellido materno</label>
                                <input type="text" class="form-control" id="apellido_m" name="apellido_m" value="{{ $alumno->apellido_m }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="name">Número de seguro social</label>
                                <input type="text" class="form-control" id="numero_ss" name="numero_ss" value="{{ $alumno->numero_ss }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="name">Carrera</label>
                                <input type="text" class="form-control" id="carrera" name="carrera" value="{{ $alumno->carrera }}" required>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-end">
                        <button type="submit" class="btn btn-danger" data-dismiss="modal" >
                            Cancelar
                        </button>
                        <button type="submit" class="btn btn-warning">
                            Editar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection