@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Grupos de la carrera {{ $carrera }}</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-success" onclick="$('#RegistrarGrupo').modal('show');" >Agregar grupo</button>
                            <table class="table table-striped" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre del grupo</th>
                                        <th>Ver</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($grupos as $grupo)
                                    <tr>
                                        <td>{{ $grupo->grupo_id }}</td>
                                        <td>{{ $grupo->grupo }}</td>
                                        <td>
                                            <a class="btn btn-info" href="{{ route('grupo-detalle', [$carrera ,$grupo->grupo]) }}" ><i class="fa fa-eye"></i></a>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-warning" onclick="$('#{{ $grupo->id }}').modal('show');">Editar</button>
                                            <div id="{{ $grupo->id }}" class="modal fade" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Editar grupo</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form method="POST" action="{{ route('grupo.update',$grupo->grupo_id) }}">
                                                                @csrf
                                                                @method('PUT') 
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Nuevo nombre del grupo</label>
                                                                                <input type="text" class="form-control" id="grupo" name="grupo" value="{{ $grupo->grupo }}" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Periodo</label>
                                                                                <select class="form-control" id="periodo" name="periodo" >
                                                                                    <option value="{{ $grupo->periodo }}" selected>{{ $grupo->periodo }}</option>
                                                                                    <option value="Enero - Abril">Enero - Abril</option>
                                                                                    <option value="Mayo - Agosto">Mayo - Agosto</option>
                                                                                    <option value="Septiembre - Diciembre">Septiembre - Diciembre</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Cuatrimestre</label>
                                                                                <select class="form-control" id="cuatrimestre" name="cuatrimestre">
                                                                                    <option value="{{ $grupo->cuatrimestre }}" selected>{{ $grupo->cuatrimestre }}</option>
                                                                                    <option value="1°">1°</option>
                                                                                    <option value="2°">2°</option>
                                                                                    <option value="3°">3°</option>
                                                                                    <option value="4°">4°</option>
                                                                                    <option value="5°">5°</option>
                                                                                    <option value="6°">6°</option>
                                                                                    <option value="7°">7°</option>
                                                                                    <option value="8°">8°</option>
                                                                                    <option value="9°">9°</option>
                                                                                    <option value="10°">10°</option>
                                                                                    <option value="11°">11°</option>
                                                                                    <option value="12°">12°</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Año</label>
                                                                                <input type="number" class="form-control" id="año" name="año" value="{{ $grupo->año }}" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Campus</label>
                                                                                <select class="form-control" id="campus_id" name="campus_id">
                                                                                    <option value="{{ $grupo->campus_id }}" selected>{{ $grupo->campus }}</option>
                                                                                    @foreach($campus as $c)    
                                                                                        <option value="{{ $c->id }}">{{ $c->campus }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Sistema</label>
                                                                                <select class="form-control" id="sistema_id" name="sistema_id">
                                                                                    <option value="{{ $grupo->sistema_id }}" selected>{{ $grupo->sistema }}</option>
                                                                                    @foreach($sistema as $s)    
                                                                                        <option value="{{ $s->id }}">{{ $s->sistema }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="name">Modalidad</label>
                                                                                <input type="hidden" class="form-control" id="carrera_id" name="carrera_id" value="{{ $carrera }}">    
                                                                                <select class="form-control" id="modalidad_id" name="modalidad_id">
                                                                                    <option value="{{ $grupo->modalidad_id }}" selected>{{ $grupo->modalidad }}</option>
                                                                                    @foreach($modalidad as $m)    
                                                                                        <option value="{{ $m->id }}">{{ $m->modalidad }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row justify-content-end">
                                                                        <button type="submit" class="btn btn-success">
                                                                            Editar
                                                                        </button>
                                                                        <p></p>
                                                                        <button class="btn btn-danger" data-bs-dismiss="modal" >
                                                                            Cancelar
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <form action="{{ route('grupo.destroy' , $grupo->grupo_id ) }}" method="post" class="d-inline">
                                                @csrf
                                                @method("DELETE")
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            No hay registros
                                            </td> 
                                            <td></td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                           
                            @if(Session::get('mensaje'))
                                <div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
                                    {{ Session::get('mensaje') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="RegistrarGrupo" class="modal fade" tabindex="-1"> 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrar grupo de la carrera {{ $carrera }}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
        <form id="form-create" action="{{ url('grupo') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Nombre del grupo</label>
                        <input type="text" class="form-control" id="grupo" name="grupo" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Periodo</label>
                        <select class="form-control" id="periodo" name="periodo">
                            <option selected>Elige el cuatrimestre</option>
                            <option value="Enero - Abril">Enero - Abril</option>
                            <option value="Mayo - Agosto">Mayo - Agosto</option>
                            <option value="Septiembre - Diciembre">Septiembre - Diciembre</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Cuatrimestre</label>
                        <select class="form-control" id="cuatrimestre" name="cuatrimestre">
                            <option selected>Elige el cuatrimestre</option>
                            <option value="1°">1°</option>
                            <option value="2°">2°</option>
                            <option value="3°">3°</option>
                            <option value="4°">4°</option>
                            <option value="5°">5°</option>
                            <option value="6°">6°</option>
                            <option value="7°">7°</option>
                            <option value="8°">8°</option>
                            <option value="9°">9°</option>
                            <option value="10°">10°</option>
                            <option value="11°">11°</option>
                            <option value="12°">12°</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Año</label>
                        <input type="number" class="form-control" id="año" name="año" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Campus</label>
                        <select class="form-control" id="campus_id" name="campus_id">
                            <option selected>Elige un campus</option>
                            @foreach($campus as $c)    
                                <option value="{{ $c->id }}">{{ $c->campus }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Sistema</label>
                        <select class="form-control" id="sistema_id" name="sistema_id">
                            <option selected>Elige un sistema</option>
                            @foreach($sistema as $s)    
                                <option value="{{ $s->id }}">{{ $s->sistema }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Modalidad</label>
                        <select class="form-control" id="modalidad_id" name="modalidad_id">
                            <option selected>Elige una modalidad</option>
                            @foreach($modalidad as $m)    
                                <option value="{{ $m->id }}">{{ $m->modalidad }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control" id="carrera_id" name="carrera_id" value="{{ $carrera }}">        
            <div class="row justify-content-end">
                <button type="submit" class="btn btn-success">
                    Registrar
                </button>
                <p></p>
                <button class="btn btn-danger" data-bs-dismiss="modal" >
                    Cancelar
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
