@extends('layouts.sidebar')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-11">
                    <div class="card">
						<div class="card-header" align="center">{{ $grupo_info-> grupo }}</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Grupo</span>
										<label class=" form-control border mr-auto" align="left">{{ $grupo_info-> grupo }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Campus</span>
										<label class=" form-control border mr-auto" align="left">{{ $campus-> campus }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Cuatrimestre</span>
										<label class=" form-control border mr-auto" align="left">{{ $grupo_info-> cuatrimestre }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Sistema</span>
										<label class=" form-control border mr-auto" align="left">{{ $sistema-> sistema }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Periodo</span>
										<label class=" form-control border mr-auto" align="left">{{ $grupo_info-> periodo }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Modalidad</span>
										<label class=" form-control border mr-auto" align="left">{{ $modalidad-> modalidad }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Año</span>
										<label class=" form-control border mr-auto" align="left">{{ $grupo_info-> año }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Carrera</span>
										<label class=" form-control border mr-auto" align="left">{{ $carrera-> carrera }}</label>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-4">
											<h4>Materias</h4>
										</div>
										<div class="col-4">
											<button type="button" class="btn btn-success" onclick="$('#AsignarMateria').modal('show');" >Asignar</button>
										</div>
									</div>
									<div class="table">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><font size=3> <i class="fa fa-book"></i> Materias </font></th>
													<th><font size=3> <i class="fa fa-chalkboard-teacher"></i> Maestros a cargo </font></th>
													<th><font size=3> <i class="fa fa-clock"></i> Hora de inicio </font></th>
													<th><font size=3> <i class="fa fa-clock"></i> Hora de tolerancia </font></th>
													<th><font size=3> <i class="fa fa-clock"></i> Hora de salida </font></th>
													<th><font size=3> <i class="fa fa-gear"></i> Acciones</th>
												</tr>
											</thead>
											<tbody>
												@forelse($maestros_materias_grupos as $m)
												<tr>
													<td>{{ $m->materia}}</td>
													<td>{{ $m->numero_personal}} - {{ $m->nombre}} {{ $m->apellido_p}} {{ $m->apellido_m}}</td>
													<td>{{ $m->hora_inicio}}</td>
													<td>{{ $m->hora_tolerancia}}</td>
													<td>{{ $m->hora_fin}}</td>
													<td>
														<button type="button" class="btn btn-danger">Eliminar</button>
														
														
													</td>
												</tr>
												@empty
												
												<tr>
													<td>
													No tiene maestros
												</td>
												</tr>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="row">
										<div class="col-4">
											<h4>Alumnos</h4>
										</div>
										<div class="col-4">
											<button type="button" class="btn btn-success" onclick="$('#AsignarAlumno').modal('show');" >Asignar</button>
										</div>
									</div>
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><font size=3> <i class="fa fa-hashtag"></i> Matricula</font></th>
													<th><font size=3> <i class="fa fa-user"></i> Nombre completo </font></th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												@forelse($alumnos_grupo as $alumno)
													<tr>
														<td>{{ $alumno->matricula}}</td>
														<td>{{ $alumno->nombre}} {{ $alumno->apellido_p}} {{ $alumno->apellido_m}}</td>
						
														<td>
															<form action="{{ route('grupo-tiene-alumnos.destroy',$alumno->id) }}" method="post" class="d-inline">
																@csrf
																@method("DELETE")
																<button type="submit" class="btn btn-danger">Remover</button>
															</form>
														</td>
													</tr>
													@empty
													
													<tr>
														<td>
														No tiene alumnos
													</td>
													</tr>
													@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row" align="center">
								<div class="col">
								<br>
								<a class="btn btn-info" href="/jefe_carrera/miscarreras/{{ $carrera-> carrera }}-grupos"><i class="fa fa-arrow-circle-left"></i>Regresar</a>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="AsignarMateria" class="modal fade" tabindex="-1"> 
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
       	 		<h5 class="modal-title">Asignar materia</h5>
        		<button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      		</div>
			<div class="modal-body">
				<form id="form-create" action="{{ url('maestro-materia-grupo') }}" method="POST">
					@csrf
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label for="name">Materia</label>
								<input type="hidden" class="form-control" id="grupo_id" name="grupo_id" value="{{ $grupo_info->id }}" required>
								<select class="form-control" id="materia_id" name="materia_id">
									<option selected>Elige una materia</option>
									@foreach($materias as $materia)    
										<option value="{{ $materia->id }}">{{ $materia->materia }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label for="name">Maestro</label>
								<select class="form-control" id="maestro_id" name="maestro_id">
									<option selected>Elige un maestro</option>
									@foreach($maestros as $maestro)    
										<option value="{{ $maestro->id }}">{{ $maestro->numero_personal }} - {{ $maestro->nombre }} {{ $maestro->apellido_p }} {{ $maestro->apellido_m }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label for="name">Hora de inicio</label>
								<input type="time" class="form-control" id="hora_inicio" name="hora_inicio" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label for="name">Hora de fin</label>
								<input type="time" class="form-control" id="hora_fin" name="hora_fin" value="22:00" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label for="name">Hora de tolerancia</label>
								<input type="time" class="form-control" id="hora_tolerancia" name="hora_tolerancia" value="22:00" required>
							</div>
						</div>
					</div>
					<div class="row justify-content-end">
						<button class="btn btn-danger" data-dismiss="modal" >
							Cancelar
						</button>

						<p></p>
						<button type="submit" class="btn btn-success">
						Asignar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="AsignarAlumno" class="modal fade" tabindex="-1"> 
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
        		<h5 class="modal-title">Agregar alumno</h5>
        		<button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      		</div>
			<div class="modal-body">
				<form id="form-create" action="{{ url('grupo-tiene-alumnos') }}" method="POST">
					@csrf
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label for="name">Alumno</label>
								<input type="hidden" class="form-control" id="grupo" name="grupo" value="{{ $grupo_info->id }}" required>
								<select class="form-control" id="matricula" name="matricula">
									<option selected>Elige un alumno</option>
									@foreach($alumnos as $alumno)    
										<option value="{{ $alumno->matricula }}">{{ $alumno->matricula }} - {{ $alumno->nombre }} {{ $alumno->apellido_p }} {{ $alumno->apellido_m }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row justify-content-end">
						<button class="btn btn-danger" data-dismiss="modal" >
							Cancelar
						</button>

						<p></p>
						<button type="submit" class="btn btn-success">
						Asignar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection