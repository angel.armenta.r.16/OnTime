@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Mis carreras</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Carrera</th>
                                        <th>Ver</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($carreras as $carrera)
                                    <tr>
                                        <td>{{ $carrera->carrera }}</td>
                                        <td>
                                            <a class="btn btn-info" href="{{ route('carrera-grupos', $carrera->carrera ) }}" >Ver grupos</a>
                                            <a class="btn btn-info" href="{{ route('carrera-materias', $carrera->carrera ) }}" >Ver materias</a>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            No hay tiene carreras asignadas
                                            </td> 
                                            <td></td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
