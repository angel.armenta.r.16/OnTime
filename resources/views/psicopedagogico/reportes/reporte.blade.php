@extends('layouts.sidebar')
@section('content')
<div class="container">
    @foreach($Info as $info)    
        <h3>Reporte asistencias de {{ $info->matricula }} - {{ $info->nombre_alumno }} {{ $info->apellido_p_alumno }} {{ $info->apellido_m_alumno }}</h3>
    @endforeach
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            @foreach($Info as $info)    
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Grupo</th>
                                            <td>{{ $info->grupo }}</td>
                                            <th scope="row">Campus</th>
                                            <td>{{ $info->campus }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Cuatrimestre</th>
                                            <td>{{ $info->cuatrimestre }}</td>
                                            <th scope="row">Modalidad</th>
                                            <td>{{ $info->modalidad }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Año</th>
                                            <td>{{ $info->año }}</td>
                                            <th scope="row">Sistema</th>
                                            <td>{{ $info->sistema }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Maestro a cargo</th>
                                            <td>{{ $info->numero_personal }} - {{ $info->nombre }} {{ $info->apellido_p }} {{ $info->apellido_m }}</td>
                                            <th scope="row">Carrera</th>
                                            <td>{{ $info->carrera }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Materia</th>
                                            <td>{{ $info->materia }}</td>
                                            <th scope="row">Alumno</th>
                                            <td>{{ $info->matricula }} - {{ $info->nombre_alumno }} {{ $info->apellido_p_alumno }} {{ $info->apellido_m_alumno }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">De</th>
                                            <td>{{ $from }}</td>
                                            <th scope="row">Hasta</th>
                                            <td>{{ $to }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endforeach
                            <script src="https://www.gstatic.com/charts/loader.js"></script>
                            <script>
                                google.charts.load('current', {packages: ['corechart']});
                                google.charts.setOnLoadCallback(drawChart);
                            
                                function drawChart() {
                                    // Define the chart to be drawn.
                                    var data = new google.visualization.DataTable();
                                    data.addColumn('string', 'Valor');
                                    data.addColumn('number', 'Porcentaje');
                                    data.addRows([
                                    
                                        ['A tiempo',{{ $AsistenciasATiempo }}],
                                        ['A tiempo',{{ $AsistenciasRetardo }}],
                                        ['Ausente',{{ $AsistenciasAusente }}],
                                    
                                    ]);
                                    var options = {
                                        width: 400,
                                        height: 300,
                                    };
                                    
                                    // Instantiate and draw the chart.
                                    var chart = new google.visualization.PieChart(document.getElementById('myPieChart'));
                                    chart.draw(data,options);

                                    var data2 = google.visualization.arrayToDataTable([
                                        ["Element", "Cantidad", { role: "style" } ],
                                        ["A tiempo", {{ $AsistenciasATiempo }}, "#b87333"],
                                        ["A tiempo", {{ $AsistenciasRetardo }}, "silver"],
                                        ["Ausente", {{ $AsistenciasAusente }}, "gold"]
                                    ]);

                                    var view = new google.visualization.DataView(data2);
                                    view.setColumns([0, 1,
                                                    { calc: "stringify",
                                                        sourceColumn: 1,
                                                        type: "string",
                                                        role: "annotation" },
                                                    2]);

                                    var options2 = {
                                        title: "Asistencias del alumno {{ $info->nombre_alumno }} {{ $info->apellido_p_alumno }} {{ $info->apellido_m_alumno }} ",
                                        width: 350,
                                        height: 250,
                                        bar: {groupWidth: "95%"},
                                        legend: { position: "none" },
                                    };
                                    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                                    chart.draw(view, options2);
                                }
                               
                            </script>
                            <!-- Identify where the chart should be drawn. -->
                            <br>
                            <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th >Total asistencias</th>
                                            <td>{{ $AsistenciasTotal }}</td>
                                           
                                        </tr>
                                        <tr>
                                            <th>A tiempo</th>
                                            <td>{{ $AsistenciasATiempo }}</td>
                                           
                                        </tr>
                                        <tr>
                                            <th >Retardos</th>

                                            <td>{{ $AsistenciasRetardo }}</td>
                                            
                                        </tr>
                                        <tr>
                                             <th >Ausencias</th>
                                            <td>{{ $AsistenciasAusente }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-6">
                                        <div id="myPieChart"></div>
                                    </div>
                                    <div class="col-6">
                                        <div id="columnchart_values"></div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
