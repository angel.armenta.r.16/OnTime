@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Reportes</h3>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <form action="/psicopedagogico/reporte/generar/alumno"  target="_blank"enctype="multipart/form-data">
                                @csrf  
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="name">Materias impartidas por maestros en grupos</label>
                                            <select class="form-control" id="mmg_id" name="mmg_id" required>
                                                <option selected>Elige una opción</option>
                                                @foreach($maestro_materia_grupo as $m_m_g)    
                                                    <option value="{{ $m_m_g->m_m_g_id }}">{{ $m_m_g->materia }} - {{ $m_m_g->grupo }} - {{ $m_m_g->numero_personal }} {{ $m_m_g->nombre }} {{ $m_m_g->apellido_p }} {{ $m_m_g->apellido_m }}<option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="name">Alumnos</label>
                                            <select class="form-control" id="alumno_matricula" name="alumno_matricula" required>
                                                <option selected>Elige a un alumno</option>
                                                @foreach($alumnos as $a)    
                                                    <option value="{{ $a->matricula }}">{{ $a->matricula }} - {{ $a->nombre }} {{ $a->apellido_p }} {{ $a->apellido_m }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="name">Fecha inicio</label>
                                            <input type="date" class="form-control" id="fecha_inicio" name="fecha_inicio" required>        
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="name">Fecha fin</label>
                                            <input type="date" class="form-control" id="fecha_fin" name="fecha_fin" required>        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary">Generar reporte</button>
                                    </div>
                                </div>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
    $('#mmg_id').select2();
    $('#sistema_id').select2();
    $('#modalidad_id').select2();
    $('#carrera_id').select2();
    $('#grupo_id').select2();
    $('#alumno_matricula').select2();
    });
</script>
@endsection
