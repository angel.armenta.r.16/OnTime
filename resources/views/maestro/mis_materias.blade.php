@extends('layouts.sidebar')

@section('content')
<div class="container">
    <h3>Mis materias</h3>
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped" style="width:100%;border:1px;" >
                                <thead>
                                    <tr>
                                        <th>Materia</th>
                                        <th>Grupo</th>
                                        <th>Ver</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($MisMaterias as $materia)
                                    <tr>
                                        <td>{{ $materia->materia }}</td>
                                        <td>{{ $materia->grupo}}</td>
                                        <td>
                                            <a class="btn btn-info" href="{{ route('materia-info', [$materia->m_m_g_id,$materia->materia ,$materia->grupo]) }}" >Ver grupos</a>   
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            No hay tiene materias asignadas
                                            </td> 
                                            <td></td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
