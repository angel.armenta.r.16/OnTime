@extends('layouts.sidebar')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-11">
                    <div class="card">
						<div class="card-header" align="center">{{ $materia-> materia }}</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Grupo</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> grupo }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Campus</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> campus }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Cuatrimestre</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> cuatrimestre }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Sistema</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> sistema }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Periodo</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> periodo }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Modalidad</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> modalidad }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Año</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> año }}</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group mb-2">
										<span class="input-group-text col-3" id="basic-addon1">Carrera</span>
										<label class=" form-control border mr-auto" align="left">{{ $materia-> carrera }}</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="row">
										<div class="col-4">
											<h4>Alumnos</h4>
										</div>
									</div>
								<div class="col-md-12" >
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><font size=3> <i class="fa fa-hashtag"></i> Matricula</font></th>
													<th><font size=3> <i class="fa fa-user"></i> Nombre completo </font></th>
													<th>Generar asistencia</th>
													<th>Generar ausencia</th>
												</tr>
											</thead>
											<tbody>
												@forelse($alumnos as $alumno)
													<tr>
														<td>{{ $alumno->matricula}}</td>
														<td>{{ $alumno->nombre}} {{ $alumno->apellido_p}} {{ $alumno->apellido_m}}</td>
														<td>
															<button type="button" class="btn btn-success" onclick="$('#{{ $alumno->matricula}}').modal('show');"><i class="fa fa-check"></i></button>
                                            				<div id="{{ $alumno->matricula}}" class="modal fade" tabindex="-1">
															<div class="modal-dialog">
																<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title">Generar asistencia del alumno {{ $alumno->matricula}} - {{ $alumno->nombre}} {{ $alumno->apellido_p}} {{ $alumno->apellido_m}}</h5>
																	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
																</div>
																<div class="modal-body">
																	<form id="form-create" action="{{ url('asistencia') }}" method="POST">
																		@csrf
																		<input type="hidden" class="form-control" id="alumno_matricula" name="alumno_matricula" value="{{ $alumno->matricula}}">        
																		<input type="hidden" class="form-control" id="maestro_materia_grupo" name="maestro_materia_grupo" value="{{ $materia-> m_m_g_id}}">        
																		<div class="row justify-content-end">
																		
																			<button type="submit" class="btn btn-success">
																				Registrar asistencia
																			</button>
																			<p></p>
																			<button type="button" class="btn btn-danger" data-dismiss="modal" >
																				Cancelar
																			</button>
																		</div>
																	</form>
																</div>
															</div>
														</td>
														<td>
															<button type="button" class="btn btn-secondary" onclick="$('#ausencia-{{ $alumno->matricula}}').modal('show');"><i class="fa fa-check"></i></button>
                                            				<div id="ausencia-{{ $alumno->matricula}}" class="modal fade" tabindex="-1">
															<div class="modal-dialog">
																<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title">Generar ausencia del alumno {{ $alumno->matricula}} - {{ $alumno->nombre}} {{ $alumno->apellido_p}} {{ $alumno->apellido_m}}</h5>
																	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
																</div>
																<div class="modal-body">
																	<form id="form-create" action="/ausencia" method="POST">
																		@csrf
																		<input type="hidden" class="form-control" id="alumno_matricula" name="alumno_matricula" value="{{ $alumno->matricula}}">        
																		<input type="hidden" class="form-control" id="maestro_materia_grupo" name="maestro_materia_grupo" value="{{ $materia-> m_m_g_id}}">        
																		<div class="row justify-content-end">
																			<button type="submit" class="btn btn-success">
																				Registrar ausencia
																			</button>
																			<p></p>
																			<button type="button" class="btn btn-danger" data-dismiss="modal" >
																				Cancelar
																			</button>
																		</div>
																	</form>
																</div>
															</div>
														</td>
													</tr>
													@empty
													<tr>
														<td>
														No tiene alumnos
													</td>
													</tr>
													@endforelse
											</tbody>
										</table>
										@if(Session::get('mensaje'))
											<div class="alert alert-{{Session::get('color-class')}} mt-3" role="alert">
												{{ Session::get('mensaje') }}
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection