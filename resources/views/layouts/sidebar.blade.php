<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ Auth::user()->name }}</title>
    <!-- ✅ Load CSS file for Select2 -->
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" rel="stylesheet"/>
    
    <!-- ✅ load jQuery ✅ -->
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>

    <!-- ✅ load JS for Select2 ✅ -->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/select2.min.js') }}" defer></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"  crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.4/css/tether.min.css'>
    <!--css de sidebar-->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
   
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <!-- Laravel PWA-->
    @laravelPWA
   
</head>

<body>
<div id="wrapper">
    <!-- Sidebar - Brand -->
    <ul class="navbar-nav bg-gradient-dark text-white sidebar sidebar-dark " id="accordionSidebar">

        <a class="sidebar-brand d-flex align-items-center justify-content-center" >
            <div class="sidebar-brand-icon ">
                <img src="{{ asset('img/logo.png') }}" style="width: 55px;">
            </div>
            <div class="sidebar-brand-text mx-3">{{ Auth::user()->name }}</div>
        </a>

        @can('campus.home')
            <!-- Divider -->
                <hr class="sidebar-divider my-1">
                <div class="sidebar-heading">
                    <i class="fas fa-fw fa-building"></i> Estructura
                </div>
            <!-- Nav Item -->
            <li class="nav-item active">
                <a class="nav-link" href="/admin/campus">
                    <i class="fas fa-fw fa-thin fa-circle"></i>
                    <span>Campus</span>
                </a>
            </li>
        @endcan
        @can('modalidad.home')
            <li class="nav-item active">
                <a class="nav-link" href="/admin/modalidades">
                    <i class="fas fa-fw fa-thin fa-circle"></i>
                    <span>Modalidades</span>
                </a>
            </li>
        @endcan
        @can('sistema.home')
            <li class="nav-item active">
                <a class="nav-link" href="/admin/sistemas">
                    <i class="fas fa-fw fa-thin fa-circle"></i>
                    <span>Sistemas</span>
                </a>
            </li>
        @endcan
        @can('carrera.home')
            <li class="nav-item active">
                <a class="nav-link" href="/admin/carreras">
                    <i class="fas fa-fw fa-thin fa-circle"></i>
                    <span>Carreras</span>
                </a>
            </li>
        @endcan
       
        @can('maestro.home')
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Empleados
        </div>
            <li class="nav-item active">
                <a class="nav-link" href="/admin/maestros">
                    <i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Maestros</span>
                </a>
            </li>
        @endcan
        @can('jefecarrera.home')
            <li class="nav-item active">
                <a class="nav-link" href="/admin/jefes_carrera">
                    <i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Jefes de carrera</span>
                </a>
            </li>
        @endcan
        <hr class="sidebar-divider">
        @can('materia.home')
            <li class="nav-item active">
                <a class="nav-link" href="/jefe_carrera/miscarreras">
                    <i class="fas fa-fw fa-book"></i>
                    <span>Carreras</span>
                </a>
            </li>
        @endcan
        @can('alumnos.home')
        <!-- Divider -->
       
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-graduation-cap"></i>
                <span>Alumnos</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Alumnos:</h6>
                    <a class="collapse-item" href="/jefe_carrera/alumnos"> Buscar alumno</a>
                    <a class="collapse-item" href="/jefe_carrera/alumnos_registro"><i class="fas fa-fw fa-user"></i> Registrar alumno</a>
                    <a class="collapse-item" href="/jefe_carrera/alumnos_registro_excel"><i class="fas fa-fw fa-users"></i> Registrar alumnos</a>
                </div>
            </div>
        </li>
        @endcan
       
        @can('reportes.home')
        <li class="nav-item active">
            <a class="nav-link" href="/psicopedagogico/reporte">
                <i class="fas fa-fw fa-book"></i>
                <span>Reportes</span>
            </a>
        </li>
        @endcan
        @can('mismaterias.home')
        <li class="nav-item active">
            <a class="nav-link" href="/maestro_materias">
                <i class="fas fa-fw fa-book"></i>
                <span>Mis materias</span>
            </a>
        </li>
        @endcan
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item active">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fas fa-fw fa-sign-out-alt"></i>
                    <span>Salir</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

        

    </ul>
    <!-- End of Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
            <main class="py-4">
                @yield('content')
            </main>
        </div>
        <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

    </div>
      

    <!-- Bootstrap core JavaScript-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>

    <!--js de sidebar-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}" defer></script>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
            
        });
        
    </script>
</body>

</html>