<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maestro_imparte_materia_grupo extends Model
{
    protected $table = 'maestro_imparte_materia_grupo';
}
