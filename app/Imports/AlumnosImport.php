<?php

namespace App\Imports;

use App\Models\Alumno;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\WithValidation;

use Throwable;
class AlumnosImport implements ToModel,WithHeadingRow,SkipsOnError, SkipsOnFailure,WithValidation
{
    use Importable, SkipsErrors, SkipsFailures;
    
    public function model(array $row)
    {
        return new Alumno([
            'matricula' => $row['matricula'],
            'nombre' => $row['nombre'],
            'apellido_p' => $row['apellido_p'],
            'apellido_m' => $row['apellido_p']
        ]);
    }

    public function rules(): array
    {
        return [
            '*.matricula' => ['unique:alumnos']
        ];
    }

    public function customValidationAttributes()
{
    return ['matricula' => 'matricula'];
}
}
