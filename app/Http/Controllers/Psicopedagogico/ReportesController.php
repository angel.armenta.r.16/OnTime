<?php


namespace App\Http\Controllers\Psicopedagogico;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campus;
use App\Models\Modalidad;
use App\Models\Sistema;
use App\Models\Carrera;
use App\Models\Alumno;
use App\Models\Grupo;
use App\Models\Asistencia;
use App\Models\Maestro_imparte_materia_grupo;
class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campus=Campus::all();
        $modalidades=Modalidad::all();
        $sistemas=Sistema::all();
        $carreras=Carrera::all();
        $grupos=Grupo::all();
        $alumnos=Alumno::all();
        $maestro_materia_grupo=Maestro_imparte_materia_grupo::join('maestros', 'maestro_imparte_materia_grupo.maestro_id', '=', 'maestros.id')
                                                            ->join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                                            ->join('grupos', 'maestro_imparte_materia_grupo.grupo_id', '=', 'grupos.id')
                                                            ->select('maestro_imparte_materia_grupo.id','maestro_imparte_materia_grupo.id as m_m_g_id',
                                                                    'materias.materia',
                                                                    'grupos.grupo', 
                                                                    'maestros.numero_personal', 'maestros.nombre', 'maestros.apellido_p','maestros.apellido_m', 
                                                                    )
                                                            ->get();
        return view('psicopedagogico.index',compact('campus','modalidades', 'sistemas', 'carreras','grupos', 'alumnos','maestro_materia_grupo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function Alumno(Request $request){
        $from = $request-> fecha_inicio;
        $to = $request-> fecha_fin;

                    $Info=Maestro_imparte_materia_grupo::join('maestros', 'maestro_imparte_materia_grupo.maestro_id', '=', 'maestros.id')
                                                ->join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                                ->join('grupos', 'maestro_imparte_materia_grupo.grupo_id', '=', 'grupos.id')
                                                    ->join('campus', 'grupos.campus_id','=','campus.id')
                                                    ->join('modalidades','grupos.modalidad_id','=','modalidades.id')
                                                    ->join('sistemas', 'grupos.sistema_id','=','sistemas.id')
                                                    ->join('carreras', 'grupos.carrera_id','=','carreras.id')
                                                    ->join('grupo_tiene_alumnos', 'grupos.id','=','maestro_imparte_materia_grupo.grupo_id')
                                                    ->join('alumnos', 'grupo_tiene_alumnos.alumno_matricula','=', 'alumnos.matricula')
                                                    ->select(
                                                    'alumnos.matricula', 'alumnos.matricula as matricula', 
                                                    'alumnos.nombre','alumnos.nombre as nombre_alumno', 
                                                    'alumnos.apellido_p','alumnos.apellido_p as apellido_p_alumno', 
                                                    'alumnos.apellido_m','alumnos.apellido_m as apellido_m_alumno',
                                                    'grupos.grupo', 'grupos.periodo', 'grupos.cuatrimestre','grupos.año',
                                                    'campus.campus', 'modalidades.modalidad', 'sistemas.sistema', 'carreras.carrera',
                                                    'maestros.numero_personal', 'maestros.nombre', 'maestros.apellido_p', 'maestros.apellido_m', 
                                                    'materias.materia', 'maestro_imparte_materia_grupo.hora_inicio', 'maestro_imparte_materia_grupo.hora_fin')
                                                ->where('maestro_imparte_materia_grupo.id',$request-> mmg_id)
                                                ->where('grupo_tiene_alumnos.alumno_matricula',$request-> alumno_matricula)
                                                ->get();
       
                    $AsistenciasTotal=Asistencia::join('alumnos', 'asistencias.alumno_matricula', '=', 'alumnos.matricula')
                                                    ->join('maestro_imparte_materia_grupo', 'asistencias.materia_maestro_grupo_id', '=', 'maestro_imparte_materia_grupo.id')
                                                    ->where('asistencias.materia_maestro_grupo_id',$request-> mmg_id)
                                                    ->where('asistencias.alumno_matricula',$request-> alumno_matricula)
                                                    ->whereBetween('fecha_dia', [$from, $to])
                                                    ->count();

                    $AsistenciasATiempo=Asistencia::join('alumnos', 'asistencias.alumno_matricula', '=', 'alumnos.matricula')
                                                    ->join('maestro_imparte_materia_grupo', 'asistencias.materia_maestro_grupo_id', '=', 'maestro_imparte_materia_grupo.id')
                                                    ->where('asistencias.materia_maestro_grupo_id',$request-> mmg_id)
                                                    ->where('asistencias.alumno_matricula',$request-> alumno_matricula)
                                                    ->where('estado', 'A tiempo')
                                                    ->whereBetween('fecha_dia', [$from, $to])
                                                    ->count();

                    $AsistenciasRetardo=Asistencia::join('alumnos', 'asistencias.alumno_matricula', '=', 'alumnos.matricula')
                                                    ->join('maestro_imparte_materia_grupo', 'asistencias.materia_maestro_grupo_id', '=', 'maestro_imparte_materia_grupo.id')
                                                    ->where('asistencias.materia_maestro_grupo_id',$request-> mmg_id)
                                                    ->where('asistencias.alumno_matricula',$request-> alumno_matricula)
                                                    ->where('estado', 'Retardo')
                                                    ->whereBetween('fecha_dia', [$from, $to])
                                                    ->count();

                    $AsistenciasAusente=Asistencia::join('alumnos', 'asistencias.alumno_matricula', '=', 'alumnos.matricula')
                                                    ->join('maestro_imparte_materia_grupo', 'asistencias.materia_maestro_grupo_id', '=', 'maestro_imparte_materia_grupo.id')
                                                    ->where('asistencias.materia_maestro_grupo_id',$request-> mmg_id)
                                                    ->where('asistencias.alumno_matricula',$request-> alumno_matricula)
                                                    ->where('estado', 'Ausente')
                                                    ->whereBetween('fecha_dia', [$from, $to])
                                                    ->count();
                
            return view('psicopedagogico\reportes\reporte',compact('Info', 'AsistenciasTotal', 'AsistenciasATiempo', 'AsistenciasRetardo', 'AsistenciasAusente','from', 'to'));
           

             
        
       
    }
}
