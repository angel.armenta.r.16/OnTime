<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, HasRoles;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo(){
        //en el metodo hasAnyRole ingresa el nombre del o los roles que se redirigen a la vista deseada
        
                if(Auth::user()->hasAnyRole('Admin')){
                    return $this->redirectTo = route('admin.campus') ;
                    
                }elseif(Auth::user()->hasAnyRole('JefeCarrera')){
                    return $this->redirectTo = route('jefecarrera.miscarreras') ;
                }elseif(Auth::user()->hasAnyRole('Maestro')){
                    return $this->redirectTo = route('maestro.mismaterias') ;
                }elseif(Auth::user()->hasAnyRole('Psicopedagogico')){
                    return $this->redirectTo = route('psico.reportes') ;
                }
            }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
