<?php

namespace App\Http\Controllers\Maestro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\Grupo;
use App\Models\Asistencia;
use App\Models\Maestro_imparte_materia_grupo;
use App\Models\Grupo_tiene_alumnos;
use Auth;
use App\Models\Maestro;

class MateriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario_id = Auth::user()->id;
        $maestro= Maestro::where('usuario_id', $usuario_id)->first();
        
      
        $MisMaterias=Maestro_imparte_materia_grupo::join('maestros', 'maestro_imparte_materia_grupo.maestro_id', '=', 'maestros.id')
                                                    ->join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                                    ->join('grupos', 'maestro_imparte_materia_grupo.grupo_id', '=', 'grupos.id')
                                                    ->select('maestro_imparte_materia_grupo.id', 'maestro_imparte_materia_grupo.id as m_m_g_id',
                                                            'materias.materia', 'materias.materia as materia',
                                                            'grupos.grupo', 'grupos.grupo as grupo')
                                                    ->where('maestro_imparte_materia_grupo.maestro_id', $maestro->id)
                                                    ->get();
        return view('maestro/mis_materias', compact('MisMaterias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $materia=Maestro_imparte_materia_grupo::join('maestros', 'maestro_imparte_materia_grupo.maestro_id', '=', 'maestros.id')
                                                    ->join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                                    ->join('grupos', 'maestro_imparte_materia_grupo.grupo_id', '=', 'grupos.id')
                                                    ->join('campus', 'grupos.campus_id','=','campus.id')
                                                    ->join('modalidades','grupos.modalidad_id','=','modalidades.id')
                                                    ->join('sistemas', 'grupos.sistema_id','=','sistemas.id')
                                                    ->join('carreras', 'grupos.carrera_id','=','carreras.id')
                                                    ->select('maestro_imparte_materia_grupo.id', 'maestro_imparte_materia_grupo.id as m_m_g_id',
                                                            'materias.materia', 'materias.materia as materia',
                                                            'grupos.id','grupos.id as grupo_id', 
                                                            'grupos.grupo','grupos.grupo as grupo', 
                                                            'grupos.periodo', 'grupos.periodo as periodo',
                                                            'grupos.cuatrimestre', 'grupos.cuatrimestre as cuatrimestre',
                                                            'grupos.año', 'grupos.año as año',
                                                            'campus.campus', 'campus.campus as campus',
                                                            'modalidades.modalidad', 'modalidades.modalidad as modalidad',
                                                            'sistemas.sistema', 'sistemas.sistema as sistema',
                                                            'carreras.carrera','carreras.carrera as carrera')
                                                    ->where('maestro_imparte_materia_grupo.id', $id)
                                                    ->first();
        $alumnos=Grupo_tiene_alumnos::join('alumnos', 'grupo_tiene_alumnos.alumno_matricula', '=', 'alumnos.matricula')
                                            ->where('grupo_id', $materia->grupo_id)
                                            ->get();
        
        return view('maestro/info-materias', compact('materia','alumnos'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
