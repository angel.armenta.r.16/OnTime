<?php

namespace App\Http\Controllers\Maestro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\Asistencia;
use App\Models\Maestro_imparte_materia_grupo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;


class AsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Genero la fecha de hoy
        $date = Carbon::now();
        //Obtengo la fecha y hora
        $hora=$date->toTimeString();
        $fecha=$date->toDateString();
        $asistencia=Asistencia::where('fecha_dia', $fecha)
                                ->where('alumno_matricula', $request->alumno_matricula)
                                ->first();
        
        $materia=Maestro_imparte_materia_grupo::join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                                ->where('maestro_imparte_materia_grupo.id',$request->maestro_materia_grupo)
                                                ->first();
                                                
        if ($hora <= $materia->hora_tolerancia && $hora >= $materia->hora_inicio) {   
            $estado = "A tiempo";
        }      
        else{
            $estado = "Retardo";
        }       
        // Si no existe, hacemos el registro.
        if (!is_object($asistencia)) {
        $asistencia = new Asistencia;
         
        $asistencia->fecha_dia = $fecha;
        $asistencia->hora = $hora;
        $asistencia->estado = $estado;
        $asistencia->alumno_matricula = $request->alumno_matricula;
        $asistencia->materia_maestro_grupo_id = $request->maestro_materia_grupo;
         if ($asistencia->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡Se ha generado la asistencia exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ya has generado la asistencia de este alumno, verifique.');
         }
 
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ausencia(Request $request){
           //Genero la fecha de hoy
        $date = Carbon::now();
        //Obtengo la fecha y hora
        $hora=$date->toTimeString();
        $fecha=$date->toDateString();
        $asistencia=Asistencia::where('fecha_dia', $fecha)
                                ->where('alumno_matricula', $request->alumno_matricula)
                                ->first();
        
        $materia=Maestro_imparte_materia_grupo::join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                                ->where('maestro_imparte_materia_grupo.id',$request->maestro_materia_grupo)
                                                ->first();
                                            
        $estado = "Ausente";
         
        // Si no existe, hacemos el registro.
        if (!is_object($asistencia)) {
        $asistencia = new Asistencia;
         
        $asistencia->fecha_dia = $fecha;
        $asistencia->hora = $hora;
        $asistencia->estado = $estado;
        $asistencia->alumno_matricula = $request->alumno_matricula;
        $asistencia->materia_maestro_grupo_id = $request->maestro_materia_grupo;
         if ($asistencia->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡Se ha generado la ausencia del alumno exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ya has generado la ausencia de este alumno, verifique.');
         }
 
         return back();
    }
}
