<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use App\Models\Carrera;
use App\Models\Jefe_carrera;
use Auth;

use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class CarreraController extends Controller
{
  
    public function index()
    {
        $usuario_id = Auth::user()->id;
        $carreras=Jefe_carrera::join('users', 'jefes_carrera.usuario_id', '=', 'users.id')
                              ->join('carreras', 'jefes_carrera.carrera_id', '=', 'carreras.id')
                              ->join('maestros', 'users.id', '=', 'maestros.usuario_id')
                              ->select('carreras.id', 'carreras.id as id_carrera',
                                        'carreras.carrera', 'carreras.carrera as carrera')
                              ->where('jefes_carrera.usuario_id', $usuario_id)
                              ->get();
        return view('jefe_carrera/carreras/index', compact('carreras'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
       //
    }

    public function show($id)
    {
       
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
       //
    }

    public function destroy($id)
    {
       //
    }

    public function ruta(Request $request, $carrera){
        $carrera=2;
        $carreras=Jefe_carrera::join('users', 'jefes_carrera.usuario_id', '=', 'users.id')
                              ->join('carreras', 'jefes_carrera.carrera_id', '=', 'carreras.id')
                              ->join('maestros', 'users.id', '=', 'maestros.usuario_id')
                              ->select('carreras.id', 'carreras.id as id_carrera',
                                        'carreras.carrera', 'carreras.carrera as carrera')
                              ->where('jefes_carrera.carrera_id', $carrera)
                              ->get();
        dd($carreras);
    }
}
