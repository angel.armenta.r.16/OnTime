<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Grupo;
use App\Models\Campus;
use App\Models\Sistema;
use App\Models\Modalidad;
use App\Models\Carrera;
use App\Models\Jefe_carrera;
use App\Models\Maestro;
use App\Models\Materia;
use App\Models\Maestro_imparte_materia_grupo;


class MaestroImparteMateriaGrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $maestro_materia_grupo=Maestro_imparte_materia_grupo::where('grupo_id', $request->grupo_id)
                                                            ->where('maestro_id', $request->maestro_id)
                                                            ->where('materia_id', $request->materia_id)
                                                            ->first();
        
        // Si no existe, hacemos el registro.
        if (!is_object($maestro_materia_grupo)) {
             
        $maestro_materia_grupo = new Maestro_imparte_materia_grupo;
         
        $maestro_materia_grupo->grupo_id = $request->grupo_id;
        $maestro_materia_grupo->maestro_id = $request->maestro_id;
        $maestro_materia_grupo->materia_id = $request->materia_id;
        $maestro_materia_grupo->hora_inicio = $request->hora_inicio;
        $maestro_materia_grupo->hora_tolerancia = $request->hora_tolerancia;
        $maestro_materia_grupo->hora_fin = $request->hora_fin;
     
        if ($maestro_materia_grupo->save()) {
        $request->session()->flash('color-class', 'success');
        $request->session()->flash('mensaje', '¡Se ha asignado el maestro exitosamente!');
        } else {
            $request->session()->flash('color-class', 'danger');
            $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
        }
        } else {
            $request->session()->flash('color-class', 'danger');
            $request->session()->flash('mensaje', 'Este maestro ya ha sido asignado, verifique.');
        }
 
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maestro_materia_grupo=Maestro_imparte_materia_grupo::findOrFail($id);
        
      
       if(is_object($maestro_materia_grupo)) {
           if($maestro_materia_grupo->delete()) {
               Session::flash('color-class', 'success');
               Session::flash('mensaje', 'El maestro eliminado exitosamente.');
               
           } else {
               Session::flash('color-class', 'danger');
               Session::flash('mensaje', 'Ocurrio un error, intente nuevamente.');
           }
       }
       
       return back();
    }
}
