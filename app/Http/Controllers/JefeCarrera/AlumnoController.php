<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carrera;
use App\Models\Alumno;
use Illuminate\Support\Facades\Session;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnos=Alumno::all();
        return view('jefe_carrera.alumnos.index', compact('alumnos'));  
    }

    /**
     * Show the form forcreating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno=Alumno::where('matricula', $request->matricula)->first();
        // Si no existe, hacemos el registro.
        if (!is_object($alumno)) {
             
        $alumno = new Alumno;
        $alumno->matricula = $request->matricula;
        $alumno->nombre = $request->nombre;
        $alumno->apellido_p = $request->apellido_p;
        $alumno->apellido_m = $request->apellido_m;
        $alumno->numero_ss = $request->numero_ss;
        $alumno->carrera = $request->carrera_id;
        
         if ($alumno->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡El alumno ha sido registrado exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'La matricula de este alumno ya existe, verifique.');
         }
 
         return redirect('/jefe_carrera/alumnos_registro');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registrar(){
        $carrera=Carrera::all();
        return view('jefe_carrera.alumnos.registrar', compact('carrera'));
    }

    public function registrarExcel(){
        return view('jefe_carrera.alumnos.registrar_excel');
    }

    public function MostrarCredencial(Request $request){
        $alumno=Alumno::where('matricula',$request->matricula)->first();
        return view('jefe_carrera.alumnos.info', compact('alumno'));
    }
}
