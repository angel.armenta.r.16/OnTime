<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Grupo;
use App\Models\Campus;
use App\Models\Sistema;
use App\Models\Modalidad;
use App\Models\Carrera;
use App\Models\Jefe_carrera;
use App\Models\Maestro;
use App\Models\Materia;
use App\Models\Maestro_imparte_materia_grupo;
use App\Models\Alumno;
use App\Models\Grupo_tiene_alumnos;
use Auth;

class GrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario_id = Auth::user()->id;
        $carrera=Jefe_carrera::join('users', 'jefes_carrera.usuario_id', '=', 'users.id')
                                    ->join('carreras', 'jefes_carrera.carrera_id', '=', 'carreras.id')
                                    ->join('maestros', 'users.id', '=', 'maestros.usuario_id')
                                    ->select('carreras.carrera', 'carreras.carrera as carrera')
                                    ->where('jefes_carrera.usuario_id', $usuario_id)
                                    ->get();
        $grupos=Grupo::paginate(5);
        $campus=Campus::all();
        $sistema=Sistema::all();
        $modalidad=Modalidad::all();
        
        return view('jefe_carrera.grupos.index', compact('grupos','campus','sistema','modalidad', 'carrera'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carrera=Carrera::where('carrera', $request->carrera_id)->first();
        $grupo=Grupo::where('grupo', $request->grupo)
                    ->where('campus_id', $request->campus_id)
                    ->where('sistema_id', $request->sistema_id)
                    ->where('modalidad_id', $request->modalidad_id)
                    ->where('carrera_id', $carrera->id)
                    ->first();
       
        // Si no existe, hacemos el registro.
        if (!is_object($grupo)) {
             
        $grupo = new Grupo;
         
        $grupo->grupo = $request->grupo;
        $grupo->periodo = $request->periodo;
        $grupo->cuatrimestre = $request->cuatrimestre;
        $grupo->año = $request->año;
        $grupo->campus_id = $request->campus_id;
        $grupo->sistema_id = $request->sistema_id;
        $grupo->modalidad_id = $request->modalidad_id;
        $grupo->carrera_id = $carrera->id;
        
         if ($grupo->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡La modalidad ha sido registrado exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'La modalidad ya existe actualmente, verifique.');
         }
 
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($carrera, $grupo)
    {
        $grupo_info=Grupo::where('grupo', $grupo)->first();
        $campus=Campus::where('id',$grupo_info->campus_id)->first();
        $sistema=Sistema::where('id',$grupo_info->sistema_id)->first();
        $modalidad=Modalidad::where('id',$grupo_info->modalidad_id)->first();
        $carrera=Carrera::where('id',$grupo_info->carrera_id)->first();

        $maestros=Maestro::all();
        $materias=Materia::where('cuatrimestre',$grupo_info->cuatrimestre)
                        ->where('carrera_id',$carrera->id)
                        ->get();

        $maestros_materias_grupos=Maestro::join('maestro_imparte_materia_grupo', 'maestro_imparte_materia_grupo.maestro_id', '=', 'maestros.id')
                                        ->join('materias', 'maestro_imparte_materia_grupo.materia_id', '=', 'materias.id')
                                        ->join('grupos', 'maestro_imparte_materia_grupo.grupo_id', '=', 'grupos.id')
                                        ->where('maestro_imparte_materia_grupo.grupo_id', $grupo_info->id)
                                        ->get();
        $alumnos = Alumno::all();
        $alumnos_grupo=Grupo_tiene_alumnos::join('alumnos', 'grupo_tiene_alumnos.alumno_matricula', '=', 'alumnos.matricula')
                                            ->where('grupo_id', $grupo_info->id)
                                            ->get();
       
        return view('jefe_carrera.grupos.grupo-info', compact('alumnos_grupo','maestros_materias_grupos','materias','maestros','grupo_info','campus','sistema','modalidad', 'carrera','alumnos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $carrera=Carrera::where('carrera', $request->carrera_id)->first();
        $grupo = Grupo::find($id);
        // Si existe, hacemos la actualización..
        if (is_object($grupo)) {
          
            $grupo->grupo = $request->grupo;
            $grupo->periodo = $request->periodo;
            $grupo->cuatrimestre = $request->cuatrimestre;
            $grupo->año = $request->año;
            $grupo->campus_id = $request->campus_id;
            $grupo->sistema_id = $request->sistema_id;
            $grupo->modalidad_id = $request->modalidad_id;
            $grupo->carrera_id = $carrera->id;
   
       if ($grupo->save()) {
          $request->session()->flash('color-class', 'success');
          $request->session()->flash('mensaje', '¡El grupo ha sido actualizado exitosamente!');
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
       }
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'El grupo ya existe, verifique.');
       }

       return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CarreraGrupos($id){
        $carrera=$id;
        $grupos=Jefe_carrera::join('carreras', 'jefes_carrera.carrera_id', '=', 'carreras.id')
                            ->join('grupos', 'grupos.carrera_id', '=', 'carreras.id')
                            ->join('campus', 'grupos.campus_id','=','campus.id')
                            ->join('modalidades','grupos.modalidad_id','=','modalidades.id')
                            ->join('sistemas', 'grupos.sistema_id','=','sistemas.id')
                            ->select('carreras.carrera', 'carreras.carrera as carrera',
                                     'grupos.id', 'grupos.id as grupo_id',
                                     'grupos.grupo', 'grupos.grupo as grupo',
                                     'grupos.cuatrimestre', 'grupos.cuatrimestre as cuatrimestre',
                                     'grupos.periodo', 'grupos.periodo as periodo', 
                                     'grupos.año', 'grupos.año as año',
                                     'sistemas.id','sistemas.id as sistema_id',
                                     'sistemas.sistema','sistemas.sistema as sistema',
                                     'modalidades.id','modalidades.id as modalidad_id',
                                     'modalidades.modalidad','modalidades.modalidad as modalidad',
                                     'campus.id','campus.id as campus_id',
                                     'campus.campus','campus.campus as campus')
                            ->where('carreras.carrera', $carrera)
                            ->get();
                            //dd($grupos);
        $campus=Campus::all();
        $sistema=Sistema::all();
        $modalidad=Modalidad::all();
        return view('jefe_carrera.grupos.index', compact('grupos','campus','sistema','modalidad','carrera'));
    }
}
