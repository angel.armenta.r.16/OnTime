<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carrera;
use App\Models\Materia;
use Illuminate\Support\Facades\Session;
use App\Models\Jefe_carrera;
use Auth;

class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario_id = Auth::user()->id;
        $carreras=Jefe_carrera::join('users', 'jefes_carrera.usuario_id', '=', 'users.id')
                              ->join('carreras', 'jefes_carrera.carrera_id', '=', 'carreras.id')
                              ->join('maestros', 'users.id', '=', 'maestros.usuario_id')
                              ->select('carreras.id', 'carreras.id as id_carrera',
                                        'carreras.carrera', 'carreras.carrera as carrera')
                              ->where('jefes_carrera.usuario_id', $usuario_id)
                              ->get();
        $materias=Materia::join('carreras', 'materias.carrera_id', '=', 'carreras.id')
                          ->get();
                         
        return view('jefe_carrera/materias/index', compact('materias','carreras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $materia=Materia::where('materia', $request->materia)->first();
        $carrera=Carrera::where('carrera', $request->carrera)->first();
        // Si no existe, hacemos el registro.

        if (!is_object($materia)) {
        $materia = new Materia;
        $materia->materia = $request->materia;
        $materia->carrera_id = $carrera->id;
        $materia->cuatrimestre = $request->cuatrimestre;
     
         if ($materia->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡La materia ha sido registrada exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'La materia ya existe actualmente, verifique.');
         }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $materia = Materia::find($id);
        $carrera=Carrera::where('carrera', $request->carrera)->first();
        // Si existe, hacemos la actualización..
        if (is_object($materia)) {
          
          $materia->materia = $request->materia;
          $materia->carrera_id = $carrera->id;
          $materia->cuatrimestre = $request->cuatrimestre;
   
       if ($materia->save()) {
          $request->session()->flash('color-class', 'success');
          $request->session()->flash('mensaje', '¡La materia ha sido actualizado exitosamente!');
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
       }
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'El nombre de la materia ya existe, verifique.');
       }

       return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materia = Materia::findOrFail($id);

        if(is_object($materia)) {
            if($materia->delete()) {
                Session::flash('color-class', 'success');
                Session::flash('mensaje', 'Materia eliminada exitosamente.');
            } else {
                Session::flash('color-class', 'danger');
                Session::flash('mensaje', 'Ocurrio un error, intente nuevamente.');
            }
        }
        return redirect('/jefe_carrera/materias');
    }

    public function CarreraMaterias($id){
        $carrera=$id;
        $materias=Materia::join('carreras', 'materias.carrera_id', '=', 'carreras.id')
                        ->where('carreras.carrera', $carrera)
                        ->select('materias.id','materias.id as materia_id',
                                 'materias.materia', 'materias.materia as materia',
                                 'materias.cuatrimestre', 'materias.cuatrimestre as cuatrimestre',
                                 'carreras.carrera', 'carreras.carrera as carrera')
                        ->get();
        
        return view('jefe_carrera/materias/index', compact('materias','carrera'));
    }
}
