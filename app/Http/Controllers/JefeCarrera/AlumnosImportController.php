<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use App\Imports\AlumnosImport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class AlumnosImportController extends Controller
{
    public function store(Request $request){
        $file = $request->file('file');
        
        $val = 1;
        $import = new AlumnosImport;
        $import->import($file);

        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }

        return back()->withStatus('Documento Excel cargado exitosamente!'); 
    }
}
