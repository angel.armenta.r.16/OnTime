<?php

namespace App\Http\Controllers\JefeCarrera;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\Grupo_tiene_alumnos;
use Illuminate\Support\Facades\Session;
class GrupoTieneAlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno_grupo=Grupo_tiene_alumnos::where('grupo_id', $request->grupo)
                                        ->where('alumno_matricula', $request->matricula)
                                        ->first();

        // Si no existe, hacemos el registro.
        if (!is_object($alumno_grupo)) {
             
        $alumno_grupo = new Grupo_tiene_alumnos;
         
        $alumno_grupo->grupo_id = $request->grupo;
        $alumno_grupo->alumno_matricula = $request->matricula;
        $alumno_grupo->status = "Activo";
        
         if ( $alumno_grupo->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡El alumno ha sido registrado en el grupo exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'El alumno ya exite en este grupo, verifique.');
         }
 
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
