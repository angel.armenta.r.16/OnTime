<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Campus;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class CampusController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:campus.index')->only('index');
        $this->middleware('can:campus.create')->only('store');
        $this->middleware('can:campus.edit')->only('update');
        $this->middleware('can:campus.destroy')->only('destroy');
    }

    public function index()
    {
        $campus=Campus::paginate(5);
        return view('administrador/campus/index', compact('campus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //Se hace una consulta a la BD buscando un campus con el mismo nombre al que se quiere insertar.
         $campus=Campus::where('campus', $request->campus)->first();
         // Si no existe, hacemos el registro.
         if (!is_object($campus)) {
         //Creamos un objeto de tipo campus
         $campus = new Campus;
         //Guardamos el valor del campo de texto de nombre campus en su campo correspondiente.
         $campus->campus = $request->campus;
         //Si el campus es insertado correctamente se muestra el mensaje de éxito.
          if ($campus->save()) {
             $request->session()->flash('color-class', 'success');
             $request->session()->flash('mensaje', '¡El campus ha sido registrado exitosamente!');
          } 
          //Si existe un error en la inserción, se muestra el mensaje.
          else {
              $request->session()->flash('color-class', 'danger');
              $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
          }
          } 
          //Si ya existe el campus, no se inserta y se muestra el mensaje.
          else {
              $request->session()->flash('color-class', 'danger');
              $request->session()->flash('mensaje', 'El campus ya existe actualmente, verifique.');
          }
  
          return redirect('/admin/campus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Verificamos si el id del campus a editar existe.
        $campus = Campus::find($id);
        
          // Si existe, hacemos la actualización..
          if (is_object($campus) ) {
            //Guardamos el valor del campo de texto de nombre campus en su campo correspondiente.
            $campus->campus = $request->campus;
        //Si el campus es actualizado correctamente se muestra el mensaje de éxito.   
         if ($campus->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡El campus ha sido actualizado exitosamente!');
         } 
         //Si ocurre un error durante la actualización, se muestra el mensaje. 
         else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } 
         //Si el nombre a editar ya existe, se muestra el mensaje.
         else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'El nombre del campus ya existe, verifique.');
         }
 
         return redirect('/admin/campus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
