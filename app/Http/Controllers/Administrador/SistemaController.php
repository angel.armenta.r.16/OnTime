<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Sistema;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class SistemaController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:sistema.index')->only('index');
        $this->middleware('can:sistema.create')->only('store');
        $this->middleware('can:sistema.edit')->only('update');
        $this->middleware('can:sistema.destroy')->only('destroy');
    }
    
    public function index()
    {
        $sistemas=Sistema::paginate(5);
        return view('administrador/sistema/index', compact('sistemas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sistema=Sistema::where('sistema', $request->sistema)->first();
        // Si no existe, hacemos el registro.
        if (!is_object($sistema)) {
             
        $sistema = new Sistema;
         
        $sistema->sistema = $request->sistema;
     
         if ($sistema->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡El sistema ha sido registrado exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'El sistema ya existe actualmente, verifique.');
         }
 
         return redirect('/admin/sistemas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sistema = Sistema::find($id);
        // Si existe, hacemos la actualización..
        if (is_object($sistema)) {
          
          $sistema->sistema = $request->sistema;
   
       if ($sistema->save()) {
          $request->session()->flash('color-class', 'success');
          $request->session()->flash('mensaje', '¡El sistema ha sido actualizado exitosamente!');
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
       }
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'El nombre del sistema ya existe, verifique.');
       }

       return redirect('/admin/sistemas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sistema = Sistema::findOrFail($id);

        if(is_object($sistema)) {
            if($sistema->delete()) {
                Session::flash('color-class', 'success');
                Session::flash('mensaje', 'Sistema eliminado exitosamente.');
            } else {
                Session::flash('color-class', 'danger');
                Session::flash('mensaje', 'Ocurrio un error, intente nuevamente.');
            }
        }
        return redirect('/admin/sistemas');
    }
}
