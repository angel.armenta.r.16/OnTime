<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Carrera;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class CarreraController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:carrera.index')->only('index');
        $this->middleware('can:carrera.create')->only('store');
        $this->middleware('can:carrera.edit')->only('update');
        $this->middleware('can:carrera.destroy')->only('destroy');
    }

    public function index()
    {
        $carreras=Carrera::paginate(5);
        return view('administrador/carreras/index', compact('carreras'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $carrera=Carrera::where('carrera', $request->carrera)->first();
        // Si no existe, hacemos el registro.
        if (!is_object($carrera)) {
             
        $carrera = new Carrera;
        $carrera->carrera = $request->carrera;
     
         if ($carrera->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡La carrera ha sido registrada exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'La carrera ya existe actualmente, verifique.');
         }
 
         return redirect('/admin/carreras');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $carrera = Carrera::find($id);
        // Si existe, hacemos la actualización..
        if (is_object($carrera)) {
          
          $carrera->carrera = $request->carrera;
   
       if ($carrera->save()) {
          $request->session()->flash('color-class', 'success');
          $request->session()->flash('mensaje', '¡La carrera ha sido actualizado exitosamente!');
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
       }
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'El nombre de la carrera ya existe, verifique.');
       }

       return redirect('/admin/carreras');
    }

    public function destroy($id)
    {
      
    }
}
