<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Modalidad;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class ModalidadController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:modalidad.index')->only('index');
        $this->middleware('can:modalidad.create')->only('store');
        $this->middleware('can:modalidad.edit')->only('update');
        $this->middleware('can:modalidad.destroy')->only('destroy');
    }

    public function index()
    {
        $modalidades=Modalidad::paginate(5);
        return view('administrador/modalidad/index', compact('modalidades'));
    }
   
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modalidad=Modalidad::where('modalidad', $request->modalidad)->first();
        // Si no existe, hacemos el registro.
        if (!is_object($modalidad)) {
             
        $modalidad = new Modalidad;
         
        $modalidad->modalidad = $request->modalidad;
     
         if ($modalidad->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡La modalidad ha sido registrado exitosamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'La modalidad ya existe actualmente, verifique.');
         }
 
         return redirect('/admin/modalidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modalidad = Modalidad::find($id);
        // Si existe, hacemos la actualización..
        if (is_object($modalidad)) {
          
          $modalidad->modalidad = $request->modalidad;
   
       if ($modalidad->save()) {
          $request->session()->flash('color-class', 'success');
          $request->session()->flash('mensaje', '¡La modalidad ha sido actualizada exitosamente!');
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
       }
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'El nombre de la modalidad ya existe, verifique.');
       }

       return redirect('/admin/modalidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
