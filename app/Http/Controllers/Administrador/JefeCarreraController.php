<?php
namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Maestro;
use App\Models\Carrera;
use App\Models\Jefe_carrera;
use Illuminate\Support\Facades\Session;


class JefeCarreraController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:jefecarrera.index')->only('index');
        $this->middleware('can:jefecarrera.create')->only('store');
        $this->middleware('can:jefecarrera.destroy')->only('destroy');
    }

    public function index()
    {
        $jefes_carrera=Jefe_carrera::join('users', 'jefes_carrera.usuario_id', '=', 'users.id')
                                    ->join('carreras', 'jefes_carrera.carrera_id', '=', 'carreras.id')
                                    ->join('maestros', 'users.id', '=', 'maestros.usuario_id')
                                    ->select('jefes_carrera.id','jefes_carrera.id as jefe_carrera_id',
                                            'maestros.numero_personal', 'maestros.numero_personal as num_personal',
                                            'maestros.nombre', 'maestros.nombre as nombre',
                                            'maestros.apellido_p', 'maestros.apellido_p as apellido_p',
                                            'maestros.apellido_m', 'maestros.apellido_m as apellido_m',
                                            'carreras.carrera', 'carreras.carrera as carrera')
                                    ->paginate(5);
                                    
        $maestros=Maestro::join('users', 'maestros.usuario_id', '=', 'users.id')
                          ->select('users.id','users.id as usuario_id',
                                   'maestros.numero_personal', 'maestros.numero_personal as num_personal',
                                   'maestros.nombre', 'maestros.nombre as nombre',
                                   'maestros.apellido_p', 'maestros.apellido_p as apellido_p',
                                   'maestros.apellido_m', 'maestros.apellido_m as apellido_m')
                          ->get();
        $carreras=Carrera::all();
        return view('administrador/jefes_carrera/index', compact('jefes_carrera','carreras','maestros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jefe_carrera=Jefe_carrera::where('carrera_id', $request->carrera_id)->first();
        $usuario=User::where('id', $request->usuario_id)->first();
        // Si no existe, hacemos el registro.
        if (!is_object($jefe_carrera)) {
             
        $jefe_carrera = new Jefe_carrera;
         
        $jefe_carrera->usuario_id = $request->usuario_id;
        $jefe_carrera->carrera_id = $request->carrera_id;
        $usuario->assignRole('JefeCarrera');
     
         if ($jefe_carrera->save()) {
            $request->session()->flash('color-class', 'success');
            $request->session()->flash('mensaje', '¡El maestro ha sido asignado a la carrera satisfactoriamente!');
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
         }
         } else {
             $request->session()->flash('color-class', 'danger');
             $request->session()->flash('mensaje', 'Esta carrera ya cuenta con un jefe de carrera, verifique.');
         }
 
         return redirect('/admin/jefes_carrera');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Seleccionar el maestro por id
        $jefe_carrera = Jefe_carrera::findOrFail($id);
         
        $usuario=User::where('id', $jefe_carrera->usuario_id)->first();
       
        if(is_object($jefe_carrera)) {
            if($jefe_carrera->delete()) {
            $usuario->removeRole('JefeCarrera');
            Session::flash('color-class', 'success');
                Session::flash('mensaje', 'Se ha destituido el jefe de carrera exitosamente.');
                
            } else {
                Session::flash('color-class', 'danger');
                Session::flash('mensaje', 'Ocurrio un error, intente nuevamente.');
            }
        }
        
        return redirect('/admin/jefes_carrera');
    }
}
