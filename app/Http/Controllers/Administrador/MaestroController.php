<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Maestro;
use App\Models\Grupo;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class MaestroController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:maestro.index')->only('index');
        $this->middleware('can:maestro.create')->only('store');
        $this->middleware('can:maestro.edit')->only('update');
        $this->middleware('can:maestro.destroy')->only('destroy');
    }

    public function index()
    {
        $maestros=Maestro::paginate(5);
        return view('administrador/maestros/index', compact('maestros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dominio='@utgz.edu.mx';
        $email=($request->email).$dominio;
        $usuario=User::where('email',$email)->first();

        if (!is_object($usuario)) {
            $user = new User;
            
            $user->name = $request->nombre;
            $user->email = $email;
            $user->password = Hash::make('Utgz2022*');
            $user->save();
            

                $user_id=User::where('email',$email)->first();
                $user_id->assignRole('Maestro');
                $maestro=Maestro::where('numero_personal', $request->numero_personal)->first();
                // Si no existe, hacemos el registro.
                if (!is_object($maestro)) {
                            $maestro = new Maestro;
                            
                            $maestro->numero_personal = $request->numero_personal;
                            $maestro->nombre = $request->nombre;
                            $maestro->apellido_p = $request->apellido_p;
                            $maestro->apellido_m = $request->apellido_m;
                            $maestro->usuario_id = $user_id->id;
                
                            if ($maestro->save()) {
                                $request->session()->flash('color-class', 'success');
                                $request->session()->flash('mensaje', '¡El maestro ha sido registrado exitosamente!');
                            } else {
                                $request->session()->flash('color-class', 'danger');
                                $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
                            }
                } else {
                    $request->session()->flash('color-class', 'danger');
                    $request->session()->flash('mensaje', 'Este maestro ya ha sido registrado, verifique.');
                }
        }
        
 
         return redirect('/admin/maestros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $maestro = Maestro::find($id);
        
        // Si existe, hacemos la actualización..
        if (is_object($maestro)) {
          
            $maestro->numero_personal = $request->numero_personal;
            $maestro->nombre = $request->nombre;
            $maestro->apellido_p = $request->apellido_p;
            $maestro->apellido_m = $request->apellido_m;
            
       if ($maestro->save()) {
          $request->session()->flash('color-class', 'success');
          $request->session()->flash('mensaje', '¡La información del maestro ha actualizada exitosamente!');
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'Ocurrio un error, vuelva a intentarlo más tarde.');
       }
       } else {
           $request->session()->flash('color-class', 'danger');
           $request->session()->flash('mensaje', 'El número de personal ya existe, verifique.');
       }

       return redirect('/admin/maestros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Seleccionar el maestro por id
        $maestro = Maestro::findOrFail($id);
        
        $usuario = User::where('id',$maestro->usuario_id)->first();
       
        if(is_object($maestro)) {
            if($maestro->delete()) {
                Session::flash('color-class', 'success');
                Session::flash('mensaje', 'El maestro eliminado exitosamente.');
                
            } else {
                Session::flash('color-class', 'danger');
                Session::flash('mensaje', 'Ocurrio un error, intente nuevamente.');
            }
        }
        if(is_object($usuario)) {
            $usuario->delete();
        }
        return redirect('/admin/maestros');
    }
}
