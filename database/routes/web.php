<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/offline', function () {
    return view('vendor/laravelpwa/offline');
});

Route::get('/', function () {
    return view('auth/login');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Rutas de administrador
//Rutas de campus
Route::get('/admin/campus','App\Http\Controllers\Administrador\CampusController@index')->middleware('can:campus.home')->name('admin.campus');
Route::resource('campus', App\Http\Controllers\Administrador\CampusController::class);

//Rutas de modalidades
Route::resource('modalidad', App\Http\Controllers\Administrador\ModalidadController::class);
Route::get('/admin/modalidades','App\Http\Controllers\Administrador\ModalidadController@index')->middleware('can:modalidad.home');

//Rutas de sistemas
Route::get('/admin/sistemas','App\Http\Controllers\Administrador\SistemaController@index')->middleware('can:sistema.home');
Route::resource('sistema', App\Http\Controllers\Administrador\SistemaController::class);

//Rutas de carreras
Route::resource('carrera', App\Http\Controllers\Administrador\CarreraController::class);
Route::get('/admin/carreras','App\Http\Controllers\Administrador\CarreraController@index')->middleware('can:carrera.home');

//Rutas de maestros
Route::get('/admin/maestros','App\Http\Controllers\Administrador\MaestroController@index')->middleware('can:maestro.home');
Route::get('admin/maestros/{slug}', ['as'=> 'maestro-detalles','uses'=> 'App\Http\Controllers\Administrador\MaestroController@show']);
Route::resource('maestro', App\Http\Controllers\Administrador\MaestroController::class);

//Rutas de maestros
Route::get('/admin/jefes_carrera','App\Http\Controllers\Administrador\JefeCarreraController@index')->middleware('can:jefecarrera.home');
Route::resource('jefescarrera', App\Http\Controllers\Administrador\JefeCarreraController::class);

//Rutas de jefe de carrera
//Rutas de alumnos
Route::resource('alumno', App\Http\Controllers\JefeCarrera\AlumnoController::class);
Route::get('/jefe_carrera/alumnos','App\Http\Controllers\JefeCarrera\AlumnoController@index');
Route::get('/jefe_carrera/alumnos_registro','App\Http\Controllers\JefeCarrera\AlumnoController@registrar');
Route::get('/jefe_carrera/alumnos_registro_excel','App\Http\Controllers\JefeCarrera\AlumnoController@registrarExcel');
//Alumnos importación mediante Excel
Route::post('/alumnos/import', 'App\Http\Controllers\JefeCarrera\AlumnosImportController@store');
Route::post('/alumnos/credencial', 'App\Http\Controllers\JefeCarrera\AlumnoController@MostrarCredencial');

//Rutas de las carreras asignadas
Route::get('/jefe_carrera/miscarreras','App\Http\Controllers\JefeCarrera\CarreraController@index')->name('jefecarrera.miscarreras');

//Rutas de materias
Route::get('/jefe_carrera/miscarreras/{carrera}-materias', ['as'=> 'carrera-materias','uses'=> 'App\Http\Controllers\JefeCarrera\MateriaController@CarreraMaterias']);
Route::resource('materia', App\Http\Controllers\JefeCarrera\MateriaController::class);

//Rutas de grupos
Route::get('/jefe_carrera/miscarreras/{carrera}-grupos', ['as'=> 'carrera-grupos','uses'=> 'App\Http\Controllers\JefeCarrera\GrupoController@CarreraGrupos']);
Route::get('/jefe_carrera/miscarreras/{carrera}-grupos/{grupo}', ['as'=> 'grupo-detalle','uses'=> 'App\Http\Controllers\JefeCarrera\GrupoController@show']);
Route::resource('grupo', App\Http\Controllers\JefeCarrera\GrupoController::class);
Route::resource('maestro-materia-grupo', App\Http\Controllers\JefeCarrera\MaestroImparteMateriaGrupoController::class);
Route::resource('grupo-tiene-alumnos', App\Http\Controllers\JefeCarrera\GrupoTieneAlumnosController::class);

//Rutas psicopedagogico
Route::get('/psicopedagogico/reporte','App\Http\Controllers\Psicopedagogico\ReportesController@index')->name('psico.reportes');
Route::get('/psicopedagogico/reporte/generar/alumno','App\Http\Controllers\Psicopedagogico\ReportesController@Alumno');
Route::get('/psicopedagogico/reporte/generar/carrera','App\Http\Controllers\Psicopedagogico\ReportesController@Carrera');

Route::get('/prueba','App\Http\Controllers\Psicopedagogico\ReportesController@prueba');

//Rutas de Maestro
Route::get('/maestro_materias','App\Http\Controllers\Maestro\MateriasController@index')->name('maestro.mismaterias');
Route::get('/maestro_materias/{id}-{materia}-{grupo}', ['as'=> 'materia-info','uses'=> 'App\Http\Controllers\Maestro\MateriasController@show']);
Route::resource('asistencia', App\Http\Controllers\Maestro\AsistenciaController::class);
Route::post('/ausencia','App\Http\Controllers\Maestro\AsistenciaController@ausencia');
