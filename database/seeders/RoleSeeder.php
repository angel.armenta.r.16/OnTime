<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creamos los roles y los almacenamos en una variable
        $admin = Role::create(['name' => 'Admin']);
        $jefe_carrera = Role::create(['name' => 'JefeCarrera']);
        $maestro = Role::create(['name' => 'Maestro']);
        $psico = Role::create(['name' => 'Psicopedagogico']);

        //Creamos los permisos para el módulo de campus y los almacenamos en una variable
        $campus_home = Permission::create(['name' => 'campus.home']);
        $campus_index = Permission::create(['name' => 'campus.index']);
        $campus_create = Permission::create(['name' => 'campus.create']);
        $campus_edit = Permission::create(['name' => 'campus.edit']);
        $campus_destroy = Permission::create(['name' => 'campus.destroy']);

        //Creamos los permisos para el módulo de modalidades y los almacenamos en una variable
        $modalidad_home = Permission::create(['name' => 'modalidad.home']);
        $modalidad_index = Permission::create(['name' => 'modalidad.index']);
        $modalidad_create = Permission::create(['name' => 'modalidad.create']);
        $modalidad_edit = Permission::create(['name' => 'modalidad.edit']);
        $modalidad_destroy = Permission::create(['name' => 'modalidad.destroy']);

        //Creamos los permisos para el módulo de sistemas y los almacenamos en una variable
        $sistema_home = Permission::create(['name' => 'sistema.home']);
        $sistema_index = Permission::create(['name' => 'sistema.index']);
        $sistema_create = Permission::create(['name' => 'sistema.create']);
        $sistema_edit = Permission::create(['name' => 'sistema.edit']);
        $sistema_destroy = Permission::create(['name' => 'sistema.destroy']);

        //Creamos los permisos para el módulo de carreras y los almacenamos en una variable
        $carrera_home = Permission::create(['name' => 'carrera.home']);
        $carrera_index = Permission::create(['name' => 'carrera.index']);
        $carrera_create = Permission::create(['name' => 'carrera.create']);
        $carrera_edit = Permission::create(['name' => 'carrera.edit']);
        $carrera_destroy = Permission::create(['name' => 'carrera.destroy']);

        //Creamos los permisos para el módulo de maestros y los almacenamos en una variable
        $maestro_home = Permission::create(['name' => 'maestro.home']);
        $maestro_index = Permission::create(['name' => 'maestro.index']);
        $maestro_create = Permission::create(['name' => 'maestro.create']);
        $maestro_edit = Permission::create(['name' => 'maestro.edit']);
        $maestro_destroy = Permission::create(['name' => 'maestro.destroy']);

        //Creamos los permisos para el módulo de jefes de carrera y los almacenamos en una variable
        $jefecarrera_home = Permission::create(['name' => 'jefecarrera.home']);
        $jefecarrera_index = Permission::create(['name' => 'jefecarrera.index']);
        $jefecarrera_create = Permission::create(['name' => 'jefecarrera.create']);
        $jefecarrera_destroy = Permission::create(['name' => 'jefecarrera.destroy']);

        //Asignamos los permisos de los módulos campus, modalidades, sistemas y carreras al rol de administrador
        $admin->syncPermissions([$campus_home, $campus_index, $campus_create,$campus_edit, $campus_destroy,
                                 $modalidad_home, $modalidad_index, $modalidad_create, $modalidad_edit, $modalidad_destroy,
                                 $sistema_home, $sistema_index, $sistema_create, $sistema_edit, $sistema_destroy,
                                 $carrera_home, $carrera_index, $carrera_create, $carrera_edit, $carrera_destroy,
                                 $maestro_home, $maestro_index, $maestro_create, $maestro_edit, $maestro_destroy,
                                 $jefecarrera_home, $jefecarrera_index, $jefecarrera_create, $jefecarrera_destroy]);

        //Creamos los permisos para el módulo de alumnos y los almacenamos en una variable
        $alumnos_home = Permission::create(['name' => 'alumnos.home']);
        $alumnos_index = Permission::create(['name' => 'alumnos.index']);
        $alumnos_create = Permission::create(['name' => 'alumnos.create']);
        $alumnos_edit = Permission::create(['name' => 'alumnos.edit']);
        $alumnos_destroy = Permission::create(['name' => 'alumnos.destroy']);

        //Creamos los permisos para el módulo de materias y los almacenamos en una variable
        $materia_home = Permission::create(['name' => 'materia.home']);
        $materia_index = Permission::create(['name' => 'materia.index']);
        $materia_create = Permission::create(['name' => 'materia.create']);
        $materia_edit = Permission::create(['name' => 'materia.edit']);
        $materia_destroy = Permission::create(['name' => 'materia.destroy']);

        //Creamos los permisos para el módulo de mis materias y los almacenamos en una variable
        $mis_materia_home = Permission::create(['name' => 'mismaterias.home']);
        $mis_materia_index = Permission::create(['name' => 'mismaterias.index']);
        
        //Asignamos los permisos del módulo Alumnos al rol de jefe de carrera
        $jefe_carrera->syncPermissions([$alumnos_home, $alumnos_index, $alumnos_create,$alumnos_edit, $alumnos_destroy,
                                        $materia_home, $materia_index, $materia_create, $materia_edit, $materia_destroy,
                                        $mis_materia_home, $mis_materia_index
                                 ]);
        //Asignamos los permisos del módulo Mis materias al rol de maestro
        $maestro->syncPermissions([ $mis_materia_home, $mis_materia_index
                                ]);
        //Creamos los permisos para el módulo de reportes y los almacenamos en una variable
        $reportes_home = Permission::create(['name' => 'reportes.home']);
        $reportes_index = Permission::create(['name' => 'reportes.index']);

        //Asignamos los permisos del módulo reportes al rol de psicopedagógico
        $psico->syncPermissions([$reportes_home, $reportes_index
                                 ]);

        
    }
}
