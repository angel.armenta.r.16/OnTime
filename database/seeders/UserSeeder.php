<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
   
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Usuario administrador
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin123')
         ])->assignRole('Admin');
        //Usuario Jefe de carrera
        User::create([
            'name' => 'JefeCarrera',
            'email' => 'jefecarrera@gmail.com',
            'password' => bcrypt('jefec123')
        ])->assignRole('JefeCarrera');
        User::create([
            'name' => 'Maestro',
            'email' => 'maestro@gmail.com',
            'password' => bcrypt('maestro123')
        ])->assignRole('Maestro');
        User::create([
            'name' => 'Psicopedagógico',
            'email' => 'psico@gmail.com',
            'password' => bcrypt('psico123')
        ])->assignRole('Psicopedagogico');
       
    }
}
