<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maestro_imparte_materia_grupo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('maestro_id')->nullable()->references('id')->on('maestros');
            $table->foreignId('materia_id')->nullable()->references('id')->on('materias');
            $table->foreignId('grupo_id')->nullable()->references('id')->on('grupos');
            $table->time('hora_inicio');
            $table->time('hora_tolerancia');
            $table->time('hora_fin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maestro_imparte_materia_grupo');
    }
};
