<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->id();
            $table->date('fecha_dia');
            $table->time('hora',0);
            $table->string('estado');
            $table->foreignId('alumno_matricula')->nullable()->references('matricula')->on('alumnos');
            $table->foreignId('materia_maestro_grupo_id')->nullable()->references('id')->on('maestro_imparte_materia_grupo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
};
