<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->id();
            $table->string('grupo');
            $table->string('cuatrimestre');
            $table->string('periodo');
            $table->integer('año');
            $table->foreignId('sistema_id')->nullable()->references('id')->on('sistemas');
            $table->foreignId('modalidad_id')->nullable()->references('id')->on('modalidades');
            $table->foreignId('campus_id')->nullable()->references('id')->on('campus');
            $table->foreignId('carrera_id')->nullable()->references('id')->on('carreras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos');
    }
};
